<div id="top"></div>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/sonibble-creators/portfolios/koso">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/31501347/Logo.png" alt="Logo" width="100" height="100">
  </a>

  <h1 align="center">Koso</h1>

  <p align="center">
    The Modern Task Management
    <br />
    <a href="https://gitlab.com/sonibble-creators/portfolios/koso"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/sonibble-creators/portfolios/koso/">View Demo</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/portfolios/koso/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/portfolios/koso/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#wiki">Wiki</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
<!-- all about the project, specify the background -->
## About The Project

<br />
<br />

<img src="screenshot/present_1.png"/>

<br />
<br />


There are many great task management tool available however, There some feature may be become unused for many task management app.

That's why **Sonibble** try to create one for making some ecosystem for the user and help by its need. If you're not knowing the sonibble please visit this, what we gonna do

Ok, There some reason user would like this system of app: 

* Help the user with most friendly and beatiful UI and UX Design. Sonibble try to make this app become amazing, and build using great color, use case and its my suited to user who want become simple.

* Fast Action like and sonic. Behind the app was create with a new technology that help user become fast and reliable without worying about the delay and time that user have.

* Offline mode Supported. The app support the offline mode, So no need worry when the connection lost any time.

Of course, no one app with suited to all of the user needs, that why we categories the need by some group. If there some interested to build this app, and have some request feature consider to contact sonibble and contribute to this project.

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



### Built With

This project build with some stack technology and service available, May there some service to updated. But for now this app work with amazing help from other stack, Here we go:

* [Flutter](https://flutter.dev)
* [Figma](https://figma.com)
* [Firebase](https://firebase.google.com)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



<!-- GETTING STARTED -->
<!-- The way to start run this project into their device -->
## Getting Started

To start this project into your device workspace very easy. This may just tak a litle bit of your time to run this project, See the detail below.

### Prerequisites

There Library, and app you need before jumping to this project. Please install the needed prerequisites.

* **Flutter**

  Flutter behind this app happen, so take a time to install the flutter into your device, see the [documentation](https://docs.flutter.dev/get-started)

* **Android Studio**

  If you are try to run this app, you need to install the android studio for your device and dont forget about the utilitize too. This needed to run your app into android device. Check the [documentation](https://developer.android.com/studio)

* **Xcode (Only for Mac Users)**

  To run this project in ios, you need a macbook and install a xcode. Check the [documentation](https://developer.apple.com/xcode)

* **Visual Studio Code (Optional)**
  If you're love the visual studio code to running your code, dont worry you can run this project into VSCode. You need to install some plugin like flutter. Check the [documentation](https://code.visualstudio.com)

### Installation

_Below is how you can run this poject with flutter as behind_

1. Clone this project, if you are trying to contribute consider to check the dev branch
   
   **Clone Using SSH**
   ```sh
   git clone git@gitlab.com:sonibble-creators/portfolios/koso.git
   ```

   **Clone Using HTTPS**
   ```sh
   git clone https://gitlab.com/sonibble-creators/portfolios/koso.git
   ```
2. Install the Dependencies
   ```sh
   flutter get
   ```
3. Run the project
   ```sh
   flutter run
   ```

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



<!-- Wiki -->
<!-- enable the user to see the wiki of this project -->
## Wiki

Wwe build this project with some record of our documentation, If you interest to see the all about this project please check the wiki.

_For more detail, please refer to the [Wiki](https://gitlab.com/sonibble-creators/portfolios/koso/-/wikis/home)_

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



<!-- ROADMAP -->
<!-- Initial info roadmap of this project -->
## Roadmap

- [x] Add Basic Feature 
- [x] Add Animation
- [x] Add Documentation
- [x] Multi-language Support
    - [x] English
    - [x] Indonesia
    - [ ] Spanish

See the [open issues](https://gitlab.com/sonibble-creators/portfolios/koso/-/issues) for a full list of proposed features (and known issues).


<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Please check the contributing procedure [here](CONTRIBUTING.md), Don't forget to give the project a star! Thanks again!


<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See [LICENSE](LICENSE.md) for more information.

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>



<!-- CONTACT -->
## Contact

Nyoman Sunima - [@nyomansunima](https://instagram.com/nyomansunima) - nyomansunima@gmail.com

Sonibble - [@sonibble](https://instagram.com/sonibble) - sonibble.pro@gmail.com

Project Link: [https://gitlab.com/sonibble-creators/portfolios/koso](https://gitlab.com/sonibble-creators/portfolios/koso)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>
