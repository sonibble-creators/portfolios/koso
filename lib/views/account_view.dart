import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:koso/components/form_input.dart';
import 'package:koso/models/account_model.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';

class AccountView extends StatefulWidget {
  const AccountView({Key? key}) : super(key: key);

  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  // variables
  // use for new updated account
  late AccountModel _newAccount = AccountModel();

  // widge variables
  final GlobalKey<FormState> _accountFormKey = GlobalKey<FormState>();

  // bind viewmodel
  final AccountViewModel _accountViewModel = Get.find();

  // appbar
  AppBar appbarWidget() => AppBar(
        title: Text("account_appbar_title".tr),
      );

  // body content
  Widget bodyContentWidget() => GetBuilder<AccountViewModel>(
      init: _accountViewModel,
      builder: (vm) {
        // we need to assign new account with model
        _newAccount = _accountViewModel.account.copyWith();

        return SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: AnimationLimiter(
            child: Column(
              children: AnimationConfiguration.toStaggeredList(
                duration: const Duration(milliseconds: 800),
                childAnimationBuilder: (widget) {
                  // give the child animation
                  return SlideAnimation(
                    horizontalOffset: 50.0,
                    child: FadeInAnimation(
                      child: widget,
                    ),
                  );
                },
                children: [
                  // avatar
                  const SizedBox(height: 20.0),
                  Center(
                    child: Stack(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(6.0),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Get.theme.dividerColor,
                            ),
                            shape: BoxShape.circle,
                          ),
                          child: _accountViewModel.account.avatar != null &&
                                  _accountViewModel.account.avatar!.isNotEmpty
                              ? CircleAvatar(
                                  radius: 44.0,
                                  backgroundImage: NetworkImage(
                                    _accountViewModel.account.avatar!,
                                  ),
                                )
                              : Container(
                                  height: 80.0,
                                  width: 80.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Get.isDarkMode
                                        ? Get.theme.primaryColorDark
                                        : Colors.white,
                                  ),
                                  child: const Center(
                                    child: Icon(
                                      IconlyBroken.user_2,
                                      size: 32.0,
                                    ),
                                  ),
                                ),
                        ),

                        // icon pick
                        GestureDetector(
                          onTap: () {
                            // pick some image to change the avatar,
                            // update the account
                            _accountViewModel.updateAvatar();
                          },
                          child: Container(
                            height: 40.0,
                            width: 40.0,
                            margin:
                                const EdgeInsets.only(top: 60.0, left: 60.0),
                            decoration: BoxDecoration(
                              color: Get.theme.primaryColor,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: const Center(
                              child: Icon(
                                IconlyBold.camera,
                                size: 20.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // the form
                  // form for update the account
                  const SizedBox(height: 28.0),
                  Form(
                    key: _accountFormKey,
                    child: Column(
                      children: [
                        FormInput(
                          key: ValueKey(_accountViewModel.account.fullName),
                          label: "account_form_fullname_label".tr,
                          hint: "account_form_fullname_hint".tr,
                          initial: _accountViewModel.account.fullName,
                          onSaved: (value) {
                            _newAccount.fullName = value;
                          },
                        ),

                        // the email cannot be updated
                        // this permanently
                        FormInput(
                          key: ValueKey(_accountViewModel.account.email),
                          enable: false,
                          margin: const EdgeInsets.only(top: 20.0),
                          label: "account_form_email_label".tr,
                          hint: "account_form_email_hint".tr,
                          initial: _accountViewModel.account.email,
                          onSaved: (value) {},
                        ),
                        TextFormInput(
                          key: ValueKey(_accountViewModel.account.bio),
                          margin: const EdgeInsets.only(top: 20.0),
                          label: "account_form_bio_label".tr,
                          hint: "account_form_bio_hint".tr,
                          initial: _accountViewModel.account.bio,
                          onSaved: (value) {
                            _newAccount.bio = value;
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(),
      body: bodyContentWidget(),
      persistentFooterButtons: [
        SizedBox.expand(
          child: ElevatedButton(
            onPressed: () {
              // we need to validate the form
              if (_accountFormKey.currentState!.validate()) {
                // we need to save the all data stored in this form
                _accountFormKey.currentState!.save();

                // after save, we maining to save the new account
                _accountViewModel.updateAccount(_newAccount);
              }
            },
            child: Text("save_button_label".tr),
          ),
        ),
      ],
    );
  }
}
