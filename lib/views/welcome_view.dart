import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:koso/data/data.dart';
import 'package:koso/views/login_view.dart';

class WelcomeView extends StatefulWidget {
  const WelcomeView({Key? key}) : super(key: key);

  @override
  _WelcomeViewState createState() => _WelcomeViewState();
}

class _WelcomeViewState extends State<WelcomeView> {
  // variables
  int _activeSlide = 0;

  // animation variables

  // widget variables
  final PageController _pageController = PageController(initialPage: 0);

  // top nav
  Widget topNavWidget() => Column(
        children: [
          const SizedBox(height: 56.0),
          Row(
            children: [
              const Spacer(),
              TextButton(
                onPressed: () {
                  Get.off(() => const LoginView());
                },
                child: Text("welcome_skip_button_label".tr),
              ),
            ],
          ),
        ],
      );

  // the into widget
  Widget introWidget() => PageView.builder(
        controller: _pageController,
        itemCount: intros.length,
        onPageChanged: (value) {
          setState(() {
            _activeSlide = value;
          });
        },
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, slidePosition) {
          // get the single, detail intro
          var intro = intros[slidePosition];

          return AnimatedOpacity(
            opacity: _activeSlide == slidePosition ? 1.0 : 0.0,
            duration: const Duration(milliseconds: 1600),
            child: AnimatedScale(
              scale: _activeSlide == slidePosition ? 1.0 : .4,
              duration: const Duration(milliseconds: 1400),
              curve: Curves.easeInOutCirc,
              alignment: Alignment.center,
              child: Column(
                children: [
                  // image for illustration
                  // this image can be used to announced to user
                  const SizedBox(height: 160.0),
                  SvgPicture.asset(
                    intro["image"]!,
                    height: 340.0,
                    fit: BoxFit.fill,
                  ),

                  // the title of the intro
                  // this may the best part we can accept
                  const SizedBox(height: 56.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Text(
                      intro["title"]!,
                      style: Get.textTheme.headline5!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),

                  // description
                  // the description of intro
                  // enable user to knowing the mean of intro
                  const SizedBox(height: 20.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Text(
                      intro["desc"]!,
                      style: Get.textTheme.bodyText2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );

  // navigation widget
  Widget navigationWidget() => Positioned(
        left: 24.0,
        right: 24.0,
        bottom: 20.0,
        child: Row(
          children: [
            // only show this if
            // the navigation is already `next`
            AnimatedOpacity(
              opacity: _activeSlide > 0 ? 1.0 : 0.0,
              duration: const Duration(milliseconds: 800),
              curve: Curves.easeInBack,
              child: TextButton(
                onPressed: () {
                  // can only next if the condition
                  // gonna be true
                  if (_activeSlide > 0) {
                    // animate into next screen
                    _pageController.animateToPage(
                      _activeSlide - 1,
                      duration: const Duration(milliseconds: 1200),
                      curve: Curves.easeInOut,
                    );
                  }
                },
                child: Text("welcome_back_button_label".tr),
              ),
            ),

            const Spacer(),
            // indicator for slider
            // the indicator and navigation
            Row(
              children: intros
                  .asMap()
                  .map(
                    (position, value) => MapEntry(
                      position,
                      AnimatedScale(
                        duration: const Duration(milliseconds: 800),
                        curve: Curves.easeIn,
                        scale: position == _activeSlide ? 1.2 : 1,
                        child: Container(
                          height: (position == _activeSlide) ? 16.0 : 10.0,
                          width: (position == _activeSlide) ? 16.0 : 10.0,
                          margin: const EdgeInsets.symmetric(horizontal: 6.0),
                          decoration: BoxDecoration(
                            color: (position == _activeSlide)
                                ? Get.theme.primaryColor
                                : const Color(0xFF9D9BAA),
                            shape: BoxShape.circle,
                          ),
                        ),
                      ),
                    ),
                  )
                  .values
                  .toList(),
            ),
            const Spacer(),
            TextButton(
              onPressed: () async {
                // skip to the last
                // and going to the login screen
                if (_activeSlide < intros.length - 1) {
                  _pageController.animateToPage(
                    _activeSlide + 1,
                    duration: const Duration(milliseconds: 1200),
                    curve: Curves.easeInOut,
                  );
                }

                if (_activeSlide == intros.length - 1) {
                  // go to login
                  await Future.delayed(const Duration(milliseconds: 800));
                  Get.off(() => const LoginView());
                }
              },
              child: Text(
                (_activeSlide == intros.length - 1)
                    ? "welcome_finish_button_label".tr
                    : "welcome_next_button_label".tr,
              ),
            ),
          ],
        ),
      );

  // body content
  Widget bodyContentWidget() => Stack(
        children: [
          introWidget(),
          navigationWidget(),
          topNavWidget(),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bodyContentWidget(),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
}
