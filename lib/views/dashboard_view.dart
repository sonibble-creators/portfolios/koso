import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:koso/components/alert.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';
import 'package:koso/viewmodels/task_viewmodel.dart';
import 'package:koso/views/account_view.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  // bind viwmodel
  final AccountViewModel _accountViewModel = Get.find();
  final TaskViewModel _taskViewModel = Get.find();

  // appbar
  AppBar appbarWidget() => AppBar(
        title: Text("dashboard_appbar_title".tr),
      );

  // account widget
  Widget accountSummaryWidget() => GetBuilder<AccountViewModel>(
      init: _accountViewModel,
      builder: (vm) {
        return GestureDetector(
          onTap: () {
            // we need to edit some profile info
            Get.to(() => const AccountView());
          },
          child: Container(
            padding: const EdgeInsets.all(16.0),
            margin: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 20.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: Get.theme.dividerColor,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(16.0),
                  child: _accountViewModel.account.avatar != null &&
                          _accountViewModel.account.avatar!.isNotEmpty
                      ? Image.network(
                          _accountViewModel.account.avatar!,
                          width: 60.0,
                          height: 60.0,
                          fit: BoxFit.cover,
                        )
                      : Container(
                          color: Colors.grey[100],
                          child: const Center(
                            child: Icon(IconlyLight.user),
                          ),
                        ),
                ),
                const SizedBox(width: 16.0),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _accountViewModel.account.fullName ?? "",
                        style: Get.textTheme.subtitle1!.copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(height: 4.0),
                      const Text("Premium Member"),
                    ],
                  ),
                ),
                IconButton(
                  onPressed: () {
                    // we need to logout from this application
                    // so user can sign again with different account
                    _accountViewModel.signOut();
                  },
                  icon: const Icon(
                    IconlyBold.logout,
                  ),
                ),
              ],
            ),
          ),
        );
      });

  // activity
  Widget activityWidget() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Text(
              "dashboard_your_activity_label".tr,
              style: Get.textTheme.headline6!.copyWith(
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          SizedBox(
            height: 145.0,
            child: AnimationLimiter(
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                children: AnimationConfiguration.toStaggeredList(
                  duration: const Duration(milliseconds: 800),
                  childAnimationBuilder: (child) {
                    return SlideAnimation(
                      horizontalOffset: 100.0,
                      child: FadeInAnimation(child: child),
                    );
                  },
                  children: [
                    Container(
                      width: 124.0,
                      margin: const EdgeInsets.only(left: 20.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: Get.theme.dividerColor,
                        ),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(height: 32.0),
                          Text(
                            _taskViewModel.countFinishedTask().toString(),
                            style: Get.textTheme.headline3!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 16.0),
                          Text(
                            "dashboard_task_finish_label".tr,
                            style: Get.textTheme.bodyText1,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 124.0,
                      margin: const EdgeInsets.only(left: 20.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: Get.theme.dividerColor,
                        ),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(height: 32.0),
                          Text(
                            _taskViewModel.countPendingTask().toString(),
                            style: Get.textTheme.headline3!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 16.0),
                          Text(
                            "dashboard_task_pending_label".tr,
                            style: Get.textTheme.bodyText1,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 124.0,
                      margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: Get.theme.dividerColor,
                        ),
                      ),
                      child: Column(
                        children: [
                          const SizedBox(height: 32.0),
                          Text(
                            _taskViewModel.countInProgressTask().toString(),
                            style: Get.textTheme.headline3!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 16.0),
                          Text(
                            "dashboard_task_inprogress_label".tr,
                            style: Get.textTheme.bodyText1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      );

  // menus
  Widget menusWidget() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 46.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Text(
              "dashboard_you_go_label".tr,
              style: Get.textTheme.headline6!.copyWith(
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          AnimationLimiter(
            child: Column(
              children: AnimationConfiguration.toStaggeredList(
                duration: const Duration(milliseconds: 800),
                childAnimationBuilder: (child) {
                  return SlideAnimation(
                    child: FadeInAnimation(child: child),
                  );
                },
                children: [
                  // the menus
                  // the main menus in dashboard
                  // can access
                  GestureDetector(
                    onTap: () {
                      // notice user
                      // this feature is not implement yet
                      Get.dialog(const NotImplementFeatureAlert());
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 24.0),
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Get.theme.dividerColor,
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Row(
                        children: [
                          // icon
                          Container(
                            height: 40.0,
                            width: 40.0,
                            decoration: BoxDecoration(
                              color: Get.theme.primaryColor,
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            child: const Center(
                                child: Icon(
                              IconlyBold.activity,
                              size: 20.0,
                              color: Colors.white,
                            )),
                          ),
                          const SizedBox(width: 16.0),
                          Text(
                            "dashboard_my_daily_label".tr,
                            style: Get.textTheme.subtitle1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      // show notice, not implement feature
                      Get.dialog(const NotImplementFeatureAlert());
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 24.0, vertical: 16.0),
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Get.theme.dividerColor,
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Row(
                        children: [
                          // icon
                          Container(
                            height: 40.0,
                            width: 40.0,
                            decoration: BoxDecoration(
                              color: Get.theme.primaryColor,
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            child: const Center(
                                child: Icon(
                              IconlyBold.calendar,
                              size: 20.0,
                              color: Colors.white,
                            )),
                          ),
                          const SizedBox(width: 16.0),
                          Text(
                            "dashboard_daily_plan_label".tr,
                            style: Get.textTheme.subtitle1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      // notice user
                      // this feature not implement yet
                      Get.dialog(const NotImplementFeatureAlert());
                    },
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 24.0),
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Get.theme.dividerColor,
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Row(
                        children: [
                          // icon
                          Container(
                            height: 40.0,
                            width: 40.0,
                            decoration: BoxDecoration(
                              color: Get.theme.primaryColor,
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            child: const Center(
                                child: Icon(
                              IconlyBold.discovery,
                              size: 20.0,
                              color: Colors.white,
                            )),
                          ),
                          const SizedBox(width: 16.0),
                          Text(
                            "dashboard_idea_label".tr,
                            style: Get.textTheme.subtitle1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // account
            accountSummaryWidget(),

            // activity
            activityWidget(),

            // the main menus
            menusWidget(),
          ],
        ),
      ),
    );
  }
}
