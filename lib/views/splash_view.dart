import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';
import 'package:koso/views/home_view.dart';
import 'package:koso/views/welcome_view.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  // initialize
  void loginChecking() async {
    // after that we going to next screen
    Future.delayed(
      const Duration(seconds: 2),
      () async {
        // we need to check the authentication for our user
        // check if the user is logout, or in
        if (await Get.find<AccountViewModel>().isSignIn()) {
          // mean that user is till in our application
          // we need to go home from here
          Get.off(() => const HomeView());
        } else {
          // the user alreadyy sign out, or no user signed to
          // our app
          // so we need to bring the user into login, and welcome
          Get.off(() => const WelcomeView());
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();

    // we need to init all of the service,
    // we use
    loginChecking();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SvgPicture.asset(
          "assets/icons/logo_default.svg",
          height: 80.0,
          width: 80.0,
        ),
      ),
    );
  }
}
