import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:koso/components/alert.dart';
import 'package:koso/components/form_input.dart';
import 'package:koso/data/data.dart';
import 'package:koso/models/task_model.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';
import 'package:koso/viewmodels/task_viewmodel.dart';

class TaskView extends StatefulWidget {
  const TaskView({Key? key}) : super(key: key);

  @override
  _TaskViewState createState() => _TaskViewState();
}

class _TaskViewState extends State<TaskView> {
  // variables
  TaskModel _newTask = TaskModel();

  // widget variables
  final GlobalKey<FormState> _taskFormKey = GlobalKey<FormState>();
  final TextEditingController _typeFormController = TextEditingController();

  // bind viewmodel
  final TaskViewModel _taskViewModel = Get.find();
  final AccountViewModel _accountViewModel = Get.find();

  // appbar
  AppBar appbarWidget() => AppBar(
        title: Text("task_appbar_title".tr),
      );

  // body content
  Widget bodyContentWidget() => SingleChildScrollView(
        padding: const EdgeInsets.all(24.0),
        child: AnimationLimiter(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: AnimationConfiguration.toStaggeredList(
              duration: const Duration(milliseconds: 1200),
              childAnimationBuilder: (child) {
                return SlideAnimation(
                  child: FadeInAnimation(child: child),
                );
              },
              children: [
                // the form going here
                Form(
                  key: _taskFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // task
                      TextFormInput(
                        key: ValueKey(
                            (_newTask.title == null || _newTask.title!.isEmpty)
                                ? "title"
                                : _newTask.title),
                        initial: _newTask.title ?? "",
                        label: "form_task_label_name".tr,
                        hint: "form_task_label_hint".tr,
                        maxLines: 3,
                        onSaved: (value) {
                          _newTask.title = value;
                        },
                      ),

                      // desc
                      TextFormInput(
                        key: ValueKey((_newTask.description == null ||
                                _newTask.description!.isEmpty)
                            ? "description"
                            : _newTask.description),
                        initial: _newTask.description ?? "",
                        margin: const EdgeInsets.only(top: 20.0),
                        label: "form_task_desc_label".tr,
                        hint: "form_task_desc_hint".tr,
                        onSaved: (value) {
                          _newTask.description = value;
                        },
                      ),

                      // deadline
                      DateFormInput(
                        key: ValueKey(_newTask.deadline ?? DateTime.now()),
                        margin: const EdgeInsets.only(top: 20.0),
                        initial: _newTask.deadline ?? DateTime.now(),
                        firstDate: DateTime(DateTime.now().year - 20),
                        lastDate: DateTime(DateTime.now().year + 20),
                        label: "form_task_deadline_label".tr,
                        hint: "form_task_deadline_hint".tr,
                        onSaved: (value) {
                          if (value != null) {
                            _newTask.deadline = value;
                          }
                        },
                      ),

                      // the task type
                      // this can be an array, start with the form and the item
                      FormInput(
                        controller: _typeFormController,
                        margin: const EdgeInsets.only(top: 20.0),
                        label: "form_task_task_type_label".tr,
                        hint: "form_task_task_type_hint".tr,
                        onSubmited: (value) {
                          // when this finish,
                          // this will triger the type and show the added type
                          _newTask.types ??= [];
                          if (!_newTask.types!.contains(value)) {
                            _newTask.types!.add(value);
                          }

                          // after we finish the add type,
                          // we are going to delete the current text in this form
                          _typeFormController.clear();

                          setState(() {});
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Wrap(
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        spacing: 12.0,
                        runSpacing: 12.0,
                        children: _newTask.types != null &&
                                _newTask.types!.isNotEmpty
                            ? _newTask.types!
                                .map(
                                  (type) => Chip(
                                    deleteIconColor:
                                        !Get.isDarkMode ? Colors.white : null,
                                    onDeleted: () {
                                      _newTask.types!.remove(type);
                                      setState(() {});
                                    },
                                    label: Text(type),
                                  ),
                                )
                                .toList()
                            : [],
                      ),

                      // colouring the task
                      // enable user to pick their favorit colors
                      const SizedBox(height: 32.0),
                      Text(
                        "form_task_colour_label".tr,
                        style: Get.textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(height: 20.0),
                      SizedBox(
                        height: 40.0,
                        child: ListView.builder(
                          itemCount: favoritedColors.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            var favColor = favoritedColors[index];
                            return GestureDetector(
                              onTap: () {
                                // set the color to selected
                                // set the selected color with
                                // code found
                                _newTask.color = favColor["code"].toString();
                                setState(() {});
                              },
                              child: (_newTask.color != null &&
                                      _newTask.color!.isNotEmpty &&
                                      _newTask.color == favColor["code"])
                                  ? Container(
                                      padding: const EdgeInsets.all(2.0),
                                      margin:
                                          const EdgeInsets.only(right: 12.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: favColor["color"] as Color,
                                          width: 2.0,
                                        ),
                                      ),
                                      child: Container(
                                        height: 24.0,
                                        width: 24.0,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: favColor["color"] as Color,
                                        ),
                                      ),
                                    )
                                  : Container(
                                      height: 28.0,
                                      width: 28.0,
                                      margin:
                                          const EdgeInsets.only(right: 12.0),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: favColor["color"] as Color,
                                      ),
                                    ),
                            );
                          },
                        ),
                      ),

                      // attach file
                      // use to attach some file if needed
                      const SizedBox(height: 36.0),
                      SizedBox(
                        width: Get.width,
                        child: OutlinedButton(
                          onPressed: () {
                            // not implement feature yet
                            Get.dialog(const NotImplementFeatureAlert());
                          },
                          child: Text("form_task_attach_file_button".tr),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  // action bottom
  Widget bottomActionButton() => SizedBox.expand(
        child: ElevatedButton(
          onPressed: () {
            //  before we go
            // we need to validate the form
            if (_taskFormKey.currentState!.validate()) {
              // save the all data
              _taskFormKey.currentState!.save();

              // add a new task
              _taskViewModel.saveTask(_newTask);
            }
          },
          child: Text("save_button_label".tr),
        ),
      );

  // initialize
  void initialize() {
    // new, create one
    // we need to add some predefined variables
    _newTask
      ..accountId = _accountViewModel.account.id
      ..status = "created"
      ..attachedFiles = []
      ..deadline = DateTime.now();
    // we are ready

    // first we gonna check the
    // task is a new, or exits (need update)
    // exist, need update
    // we are going to get data passed
    if (Get.arguments != null) {
      if (Get.arguments["task"] != null) {
        _newTask = Get.arguments["task"];
      }
    }
  }

  @override
  void initState() {
    super.initState();

    initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(),
      body: bodyContentWidget(),
      persistentFooterButtons: [
        bottomActionButton(),
      ],
    );
  }
}
