import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:koso/components/alert.dart';
import 'package:koso/themes/behavior.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  // bind viewmodel
  final AccountViewModel _accountViewModel = Get.find();

  // body content
  Widget bodyContentWidget() => CustomScrollView(
        scrollBehavior: CleanScrollBehavior(),
        slivers: [
          // appbar, as sliver
          const SliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SingleChildScrollView(
                  child: AnimationLimiter(
                    child: Column(
                      children: AnimationConfiguration.toStaggeredList(
                        duration: const Duration(milliseconds: 1200),
                        childAnimationBuilder: (child) {
                          return SlideAnimation(
                            verticalOffset: 160.0,
                            curve: Curves.easeInOutCirc,
                            child: FadeInAnimation(
                              curve: Curves.easeIn,
                              delay: const Duration(milliseconds: 60),
                              child: child,
                            ),
                          );
                        },
                        children: [
                          // the illustration
                          // knowing the illustration show intro user
                          const SizedBox(height: 40.0),
                          SvgPicture.asset(
                            "assets/images/music-celebrate.svg",
                            height: 370.0,
                            fit: BoxFit.fill,
                          ),

                          // the title
                          const SizedBox(height: 40.0),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 40.0),
                            child: Text(
                              "login_title".tr,
                              style: Get.textTheme.headline5,
                            ),
                          ),

                          // desc
                          const SizedBox(height: 20.0),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 40.0),
                            child: Text(
                              "login_desc".tr,
                              style: Get.textTheme.bodyText2,
                              textAlign: TextAlign.center,
                            ),
                          ),

                          // google login
                          // enable user to login
                          // using google account
                          const SizedBox(height: 48.0),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 24.0),
                            child: SizedBox(
                              width: Get.width,
                              child: ElevatedButton(
                                onPressed: () async {
                                  await _accountViewModel.signInWithGoogle();
                                },
                                child: const Text("Google"),
                              ),
                            ),
                          ),
                          const SizedBox(height: 40.0),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bodyContentWidget(),
    );
  }
}
