import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:koso/components/task_item_card.dart';
import 'package:koso/themes/behavior.dart';
import 'package:koso/viewmodels/notification_viewmodel.dart';
import 'package:koso/viewmodels/task_viewmodel.dart';
import 'package:koso/views/dashboard_view.dart';
import 'package:koso/views/notification_view.dart';
import 'package:koso/views/task_view.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  // variables
  // tabs
  // predefined tabs, use to show the tabs
  var tabs = ["Today", "Upcoming", "Pending", "Done", "All"];

  // state variables
  bool onSearch = false;

  // widget variables
  final TextEditingController _taskSearchController = TextEditingController();

  // bind viewmodel
  final TaskViewModel _taskViewModel = Get.find();

  // sliver list widget
  Widget sliverListHeaderWidget() => SliverList(
        delegate: SliverChildListDelegate(
          [
            SingleChildScrollView(
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24.0, vertical: 16.0),
                      child: Row(
                        children: [
                          // dashboard button
                          // the dashboard iconn button
                          // going to dashboard
                          GestureDetector(
                            onTap: () {
                              Get.to(() => const DashboardView());
                            },
                            child: Container(
                              height: 40.0,
                              width: 40.0,
                              decoration: BoxDecoration(
                                color: Get.theme.primaryColor,
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              child: const Center(
                                  child: Icon(
                                IconlyBold.category,
                                size: 20.0,
                                color: Colors.white,
                              )),
                            ),
                          ),

                          // the title
                          const Spacer(),
                          Text(
                            "Task Manager",
                            style: Get.textTheme.subtitle1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          const Spacer(),

                          // notification button
                          // the button to going into notification
                          GestureDetector(
                            onTap: () {
                              Get.to(() => const NotitficationView());
                            },
                            child: GetBuilder(
                                init: Get.find<NotificationViewModel>(),
                                builder: (_) {
                                  return Stack(
                                    children: [
                                      // icon
                                      const Icon(
                                        IconlyLight.notification,
                                        size: 28.0,
                                      ),

                                      // the badge
                                      // only show when there some notification we have
                                      if (Get.find<NotificationViewModel>()
                                          .notifications
                                          .isNotEmpty) ...[
                                        Positioned(
                                          right: 4.0,
                                          top: 4,
                                          child: Container(
                                            width: 10.0,
                                            height: 10.0,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color:
                                                  Get.theme.colorScheme.error,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ],
                                  );
                                }),
                          )
                        ],
                      ),
                    ),

                    // the search widget
                    // the widget for searching and the greating
                    const SizedBox(height: 24.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24.0),
                      child: Row(
                        children: [
                          // geating
                          // show greating into user
                          // sho this item normaly and when the state mode is being normal
                          Expanded(
                            child: AnimatedCrossFade(
                              firstChild: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "welcome_headline".tr,
                                    style: Get.textTheme.subtitle1!.copyWith(
                                      fontWeight: FontWeight.w300,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  const SizedBox(height: 8.0),
                                  Text(
                                    "welcome_tag".tr,
                                    style: Get.textTheme.headline6!.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              secondChild: TextField(
                                key: widget.key,
                                controller: _taskSearchController,
                                style: Get.textTheme.subtitle2,
                                cursorColor: Get.theme.primaryColor,
                                enableInteractiveSelection: false,
                                onSubmitted: (value) {
                                  // when the value we search empty, or null
                                  // we would be close the search form
                                  if (value.isEmpty) {
                                    onSearch = false;
                                  }

                                  setState(() {});
                                },
                                decoration: InputDecoration(
                                  hintText: "Search the task",
                                  hintStyle: Get.textTheme.bodyText2!.copyWith(
                                    color: Get.theme.hintColor,
                                  ),
                                  border: InputBorder.none,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    borderSide: BorderSide(
                                      color: Get.theme.dividerColor,
                                      width: 1,
                                    ),
                                  ),
                                  focusColor: Get.theme.focusColor,
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 16.0),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    borderSide: BorderSide(
                                      color: Get.theme.primaryColor,
                                      width: 1.4,
                                    ),
                                  ),
                                ),
                              ),
                              crossFadeState: onSearch
                                  ? CrossFadeState.showSecond
                                  : CrossFadeState.showFirst,
                              duration: const Duration(milliseconds: 400),
                              firstCurve: Curves.easeInOut,
                              secondCurve: Curves.easeInOut,
                            ),
                          ),

                          // the search button
                          // use to search some stuff
                          const SizedBox(width: 16.0),
                          GestureDetector(
                            onTap: () {
                              // set the search
                              // we need to set the mode into
                              // on search mode
                              onSearch = !onSearch;
                              setState(() {});
                            },
                            child: Container(
                              height: 44.0,
                              width: 44.0,
                              decoration: BoxDecoration(
                                color: Get.theme.primaryColor,
                                borderRadius: BorderRadius.circular(16.0),
                              ),
                              child: const Center(
                                child: Icon(
                                  IconlyLight.search,
                                  size: 20.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    // the tabs
                    // fancy tab
                    const SizedBox(height: 32.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24.0),
                      child: SizedBox(
                        height: 40.0,
                        child: TabBar(
                          isScrollable: true,
                          tabs: tabs
                              .map((String name) => Tab(text: name))
                              .toList(),
                        ),
                      ),
                    ),
                    const SizedBox(height: 16.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  // empty task
  Widget emptyTaskWidget() => SliverList(
        delegate: SliverChildListDelegate(
          [
            AnimationLimiter(
              child: Column(
                children: AnimationConfiguration.toStaggeredList(
                  childAnimationBuilder: (child) {
                    return SlideAnimation(
                      child: FadeInAnimation(child: child),
                    );
                  },
                  children: [
                    // we have illustartion
                    // showing the user about what mean
                    const SizedBox(height: 40.0),
                    SvgPicture.asset(
                      "assets/images/empty_list.svg",
                      height: 336.0,
                    ),

                    // title
                    // show the user about what mean
                    const SizedBox(height: 24.0),
                    Text(
                      "empty_task_title".tr,
                      style: Get.textTheme.headline6!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),

                    // description
                    // the description of content
                    const SizedBox(height: 16.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: Text(
                        "empty_task_desc".tr,
                        style: Get.textTheme.bodyText1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  // body tab view
  Widget bodyTabViewWidget() => GetBuilder<TaskViewModel>(
        init: _taskViewModel,
        builder: (controller) {
          return TabBarView(
            children: tabs.map((String name) {
              return SafeArea(
                top: false,
                bottom: false,
                child: Builder(
                  builder: (BuildContext context) {
                    // define the condition for each tabs setting
                    // specify the conditionedName

                    // first we need to define the condition to filtering task
                    var tasks = _taskViewModel.filterTask(
                      name,
                      query: _taskSearchController.text,
                    );

                    return CustomScrollView(
                      scrollBehavior: CleanScrollBehavior(),
                      key: PageStorageKey<String>(name),
                      slivers: <Widget>[
                        SliverOverlapInjector(
                          handle:
                              NestedScrollView.sliverOverlapAbsorberHandleFor(
                                  context),
                        ),

                        // show only when the task not empty
                        // the task not null
                        if (tasks.isNotEmpty) ...[
                          // list task
                          SliverPadding(
                            padding: const EdgeInsets.all(20.0),
                            sliver: AnimationLimiter(
                              child: SliverList(
                                delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                    var task = tasks[index];

                                    // we are going to add some animtion, like stagered

                                    return AnimationConfiguration.staggeredList(
                                      position: index,
                                      duration:
                                          const Duration(milliseconds: 800),
                                      child: SlideAnimation(
                                        horizontalOffset: 50.0,
                                        verticalOffset: 50.0,
                                        child: FadeInAnimation(
                                          child: TaskItemCard(
                                            task: task,
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  childCount: tasks.length,
                                ),
                              ),
                            ),
                          ),
                        ],

                        // if task going null or empty
                        // we will show the empty state for it
                        if (tasks.isEmpty) ...[
                          // empty task
                          emptyTaskWidget(),
                        ],
                      ],
                    );
                  },
                ),
              );
            }).toList(),
          );
        },
      );

  // main content
  Widget mainContentWidget() => DefaultTabController(
        length: tabs.length,
        child: Scaffold(
          body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverOverlapAbsorber(
                  handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: sliverListHeaderWidget(),
                ),
              ];
            },
            body: bodyTabViewWidget(),
          ),
        ),
      );

  // add task button
  Widget addTaskButtonWidget() => Align(
        alignment: Alignment.bottomCenter,
        child: GestureDetector(
          onTap: () {
            Get.to(() => const TaskView());
          },
          child: Container(
            height: 56.0,
            width: 126.0,
            margin: const EdgeInsets.only(bottom: 20.0),
            decoration: BoxDecoration(
              color: Get.theme.primaryColor,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                BoxShadow(
                  color: const Color(0xFF121021).withOpacity(0.3),
                  offset: const Offset(0, 10),
                  blurRadius: 30.0,
                ),
              ],
            ),
            child: Center(
              child: Text(
                "add_task_button".tr,
                style: Get.textTheme.button!.copyWith(
                  fontWeight: FontWeight.w600,
                  color: Get.theme.backgroundColor,
                ),
              ),
            ),
          ),
        ),
      );

  // body content
  // the main for all contebt
  Widget bodyContentWidget() => Stack(
        children: [
          // the main content
          // this contain a sliver widget
          mainContentWidget(),

          // this for add task Button
          addTaskButtonWidget(),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bodyContentWidget(),
    );
  }
}
