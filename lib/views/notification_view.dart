import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:koso/components/notification_item_card.dart';
import 'package:koso/themes/behavior.dart';
import 'package:koso/viewmodels/notification_viewmodel.dart';
import 'package:koso/views/task_view.dart';

class NotitficationView extends StatefulWidget {
  const NotitficationView({Key? key}) : super(key: key);

  @override
  _NotitficationViewState createState() => _NotitficationViewState();
}

class _NotitficationViewState extends State<NotitficationView> {
  // bind viewmodel
  final NotificationViewModel _notificationViewModel = Get.find();

  // appbar
  AppBar appbarWidget() => AppBar(
        title: Text("notification_appbar_title".tr),
      );

  // empty notification state
  Widget emptyStateNotification() => AnimationLimiter(
        child: Column(
          children: AnimationConfiguration.toStaggeredList(
            duration: const Duration(milliseconds: 600),
            childAnimationBuilder: (child) {
              return SlideAnimation(
                horizontalOffset: 100.0,
                verticalOffset: 80.0,
                child: FadeInAnimation(
                  child: child,
                ),
              );
            },
            children: [
              // we have illustartion
              // showing the user about what mean
              const SizedBox(height: 40.0),
              SvgPicture.asset(
                "assets/images/run_healty.svg",
                height: 336.0,
              ),

              // title
              // show the user about what mean
              const SizedBox(height: 24.0),
              Text(
                "empty_notification_title".tr,
                style: Get.textTheme.headline6!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),

              // description
              // the description of content
              const SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  "empty_notification_desc".tr,
                  style: Get.textTheme.bodyText1,
                  textAlign: TextAlign.center,
                ),
              ),

              // add task button
              // use to add the task
              const SizedBox(height: 48.0),
              SizedBox(
                width: 247.0,
                child: ElevatedButton(
                  onPressed: () async {
                    // we need to back, and going to notification view
                    Get.back();
                    await Future.delayed(const Duration(milliseconds: 700));
                    Get.to(() => const TaskView());
                  },
                  child: Text("add_task_button".tr),
                ),
              )
            ],
          ),
        ),
      );

  // today notification
  Widget todayNotifWidget() {
    var todayNotifs = _notificationViewModel.todayNotificationList();

    return Column(
      children: [
        // the header of list
        // contain title and button to clear
        const SizedBox(height: 40.0),
        Row(
          children: [
            Text(
              "Today",
              style: Get.textTheme.headline6!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
          ],
        ),

        // the main content
        // the list today notif
        const SizedBox(height: 36.0),
        AnimationLimiter(
          child: ListView.separated(
            itemCount: todayNotifs.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 800),
                child: SlideAnimation(
                  horizontalOffset: 50.0,
                  verticalOffset: 20.0,
                  child: FadeInAnimation(
                    child: NotificationItemCard(
                      notification: todayNotifs[index],
                    ),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return const Divider();
            },
          ),
        )
      ],
    );
  }

  // week notification
  Widget weekNotifWidget() {
    var weekNotifs = _notificationViewModel.weekNotificationList();

    return Column(
      children: [
        // the header of list
        // contain title and button to clear
        const SizedBox(height: 40.0),
        Row(
          children: [
            Text(
              "This Week",
              style: Get.textTheme.headline6!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
          ],
        ),

        // the main content
        // the list today notif
        const SizedBox(height: 36.0),
        AnimationLimiter(
          child: ListView.separated(
            itemCount: weekNotifs.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 800),
                child: SlideAnimation(
                  horizontalOffset: 50.0,
                  verticalOffset: 20.0,
                  child: FadeInAnimation(
                    child: NotificationItemCard(
                      notification: weekNotifs[index],
                    ),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return const Divider();
            },
          ),
        )
      ],
    );
  }

  // body content
  Widget bodyContentWidget() => ScrollConfiguration(
        behavior: CleanScrollBehavior(),
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            children: [
              // show when there some notification exist
              // not matter little
              if (_notificationViewModel.notifications.isNotEmpty) ...[
                // today notif
                todayNotifWidget(),

                // week notification
                weekNotifWidget(),
              ],

              // show when all the notification is empty
              // also show when nothing hapens and empty
              if (_notificationViewModel.notifications.isEmpty) ...[
                emptyStateNotification(),
              ],
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationViewModel>(
      init: _notificationViewModel,
      builder: (_) {
        return Scaffold(
          appBar: appbarWidget(),
          body: bodyContentWidget(),
        );
      },
    );
  }
}
