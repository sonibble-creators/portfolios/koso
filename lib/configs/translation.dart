import 'package:get/get.dart';

class KosoTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        // defien the translation of our app, this depend on
        // each country we supported

        // for US translation
        "en_US": {
          // login
          "login_title": "We develop your activity",
          "login_desc":
              "Access your freedom, manage all of your task in one place",

          // home
          "welcome_headline": "Welcome Back!",
          "welcome_tag": "Here’s Update Today",

          // task
          "empty_task_title": "No Task Found",
          "empty_task_desc":
              "Nice, you dont have any notification, doing something stuff, and get the new record for your life",
          "add_task_button": "Add Task",

          // other
          "save_button_label": "Save",

          // alert
          "sign_alert_title": "Sign you in",
          "sign_alert_desc":
              "Please wait, we are still checking your data and prepare your self",
          "failed_alert_title": "Opps, Failed to sign in",
          "failed_alert_desc":
              "Sorry, there some problem when trying to signin, make sure you choose your account and the process complete",
          "not_implement_alert_title": "Opps, This Feature still in develop",
          "not_implement_alert_desc":
              "We still working for this feature, we try to serve the best feature for you. Make your life easier",
          "image_source_alert_title": "Pick Some Image",
          "image_source_alert_desc":
              "Find your best image you want to show, try to get the image where ever you want",
          "image_source_alert_camera_button_label": "Camera",
          "image_source_alert_galery_button_label": "Galery",
          "save_data_alert_title": "Saving your data",
          "save_data_alert_desc":
              "Hmm, Please wait, we are trying to save your data, enjoy",
          "failed_save_data_alert_title": "Opps, Failed to save your data",
          "failed_save_data_alert_desc":
              "There some problem when we are trying to save your data, please try again, or ensure you finish the form",

          // notification
          "notification_appbar_title": "Notification",
          "empty_notification_title": "Hmm, No Notification found",
          "empty_notification_desc":
              "Nice, you dont have any notification, doing something stuff, and get the new record for your life",

          // task form
          "task_appbar_title": "Task",
          "form_task_label_name": "My New Task",
          "form_task_label_hint": "Your task",
          "form_task_desc_label": "Describe Your Task",
          "form_task_desc_hint": "What you gonna do?",
          "form_task_deadline_label": "Deadline",
          "form_task_deadline_hint": "Deadline",
          "form_task_task_type_label": "Task Type",
          "form_task_task_type_hint": "Add task type",
          "form_task_colour_label": "Colouring your task",
          "form_task_attach_file_button": "Attach File",

          // dashboard
          "dashboard_appbar_title": "Dashboard",
          "dashboard_your_activity_label": "Your Activity",
          "dashboard_task_finish_label": "Task Finished",
          "dashboard_task_pending_label": "Task Pending",
          "dashboard_task_inprogress_label": "In Progress",
          "dashboard_you_go_label": "Here You Go",
          "dashboard_my_daily_label": "My Daily",
          "dashboard_daily_plan_label": "Your Daily Plan",
          "dashboard_idea_label": "Your Idea",

          // account
          "account_appbar_title": "Account",
          "account_form_fullname_label": "Full Name",
          "account_form_fullname_hint": "Your full name",
          "account_form_email_label": "Email",
          "account_form_email_hint": "Your mail address",
          "account_form_bio_label": "Bio",
          "account_form_bio_hint": "Describe your self",

          // welcome
          "welcome_back_button_label": "Back",
          "welcome_next_button_label": "Next",
          "welcome_finish_button_label": "Finish",
          "welcome_skip_button_label": "Skip",
        },

        // for ID translation
        "id_ID": {
          // login
          "login_title": "Kami Membangun Aktivitas Kamu",
          "login_desc":
              "Akses Kebebasan Kamu, Atur semua tugas kamu hanya di satu tempat",

          // home
          "welcome_headline": "Kembali Lagi",
          "welcome_tag": "Semuanya Di Hari Ini",

          // task
          "empty_task_title": "Tidak Ada Tugas",
          "empty_task_desc":
              "Nice, kamu tidak ada tugas, lakukan sesuatu yang berharga, dan dapatkan nilai terbaik untuk hidupmu",
          "add_task_button": "Tambah Tugas",

          // other
          "save_button_label": "Simpan",

          // alert
          "sign_alert_title": "Membawamu Masuk",
          "sign_alert_desc": "Mohon menunggu, kami sedang memproses data kamu",
          "failed_alert_title": "Opps, Gagal masuk",
          "failed_alert_desc":
              "Maaf, ada masalah saat mencoba masuk, pastikan kamu memilih akun yang benar dan menyelesaikan proses",
          "not_implement_alert_title":
              "Opps, Fitur ini masih dalam pengembangan",
          "not_implement_alert_desc":
              "Kami masih mengembangkan fitur ini, kami mencoba untuk menyediakan fitur terbaik untuk kamu. Buat hidup kamu yang lebih baik",
          "image_source_alert_title": "Ambil Gambar",
          "image_source_alert_desc":
              "Temukan gambar terbaik kamu, coba temukan dimanapun gambar itu berada",
          "image_source_alert_camera_button_label": "Kamera",
          "image_source_alert_galery_button_label": "Galeri",
          "save_data_alert_title": "Menyimpan data kamu",
          "save_data_alert_desc":
              "Hmm, Mohon tunggu, kamu sedang mencoba menyimpan data kamu, Selamat Menikmati",
          "failed_save_data_alert_title": "Opps, Gagal menyimpan data kamu",
          "failed_save_data_alert_desc":
              "Ada beberapa masalah saat kami mencoba menyimpan data kamu, Coba ulangi kembali atau pastikan kamu melengkapi semua data yang diperlukan",

          // notification
          "notification_appbar_title": "Notifikasi",
          "empty_notification_title": "Hmm, Tidak Ada Notifikasi",
          "empty_notification_desc":
              "Nice, kamu tidak ada tugas, lakukan sesuatu yang berharga, dan dapatkan nilai terbaik untuk hidupmu",

          // task form
          "task_appbar_title": "Tugas",
          "form_task_label_name": "Tugas Baruku",
          "form_task_label_hint": "Tugas Kamu",
          "form_task_desc_label": "Ceritakan Tugas Kamu",
          "form_task_desc_hint": "Apa yang akan kamu lakukan?",
          "form_task_deadline_label": "Deadline",
          "form_task_deadline_hint": "Deadline",
          "form_task_task_type_label": "Jenis Tugas",
          "form_task_task_type_hint": "Tambahkan Jenis Tugas",
          "form_task_colour_label": "Warnai tugas kamu",
          "form_task_attach_file_button": "Kaitkan file",

          // dashboard
          "dashboard_appbar_title": "Beranda",
          "dashboard_your_activity_label": "Aktivitas Kamu",
          "dashboard_task_finish_label": "Tugas Selesai",
          "dashboard_task_pending_label": "Tugas Pending",
          "dashboard_task_inprogress_label": "Tugas Diproses",
          "dashboard_you_go_label": "Mari kita mulai",
          "dashboard_my_daily_label": "Harian Kamu",
          "dashboard_daily_plan_label": "Rencana Harian Kamu",
          "dashboard_idea_label": "Ide Kamu",

          // account
          "account_appbar_title": "Akun",
          "account_form_fullname_label": "Nama Lengkap",
          "account_form_fullname_hint": "Nama lengkap kamu",
          "account_form_email_label": "Email",
          "account_form_email_hint": "Alamat email kamu",
          "account_form_bio_label": "Bio",
          "account_form_bio_hint": "Ceritakan diri kamu",

          // welcome
          "welcome_back_button_label": "Kembali",
          "welcome_next_button_label": "Selanjutnya",
          "welcome_finish_button_label": "Selesai",
          "welcome_skip_button_label": "Lewati",
        },
      };
}
