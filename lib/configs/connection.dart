import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:koso/models/account_model.dart';
import 'package:koso/models/task_model.dart';

class KosoConnection {
  var auth = FirebaseAuth.instance;
  var firestore = FirebaseFirestore.instance;
  var storage = FirebaseStorage.instance;
  var messaging = FirebaseMessaging.instance;

  // ref for the firestore

  var accountRef = FirebaseFirestore.instance
      .collection("account")
      .withConverter(
          fromFirestore: (snapshot, options) =>
              AccountModel.fromMap(snapshot.id, snapshot.data()!),
          toFirestore: (value, options) => value.toMap());

  var taskRef = FirebaseFirestore.instance.collection("tasks").withConverter(
      fromFirestore: (snapshot, options) =>
          TaskModel.fromMap(snapshot.id, snapshot.data()!),
      toFirestore: (value, options) => value.toMap());
}
