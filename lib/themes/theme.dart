import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

// color variables
// define the colors we gonna use for app
const primaryColor = Color(0xFF0A0913);
const textColor = Color(0xFFECECEC);
const darkTextColor = Color(0xFFBEBECD);
const darkModePrimaryColor = Color(0xFF1F1C30);
const darkModeBackgroundColor = Color(0xFF121021);
const darkModeStatusBarColor = Color(0xFFCECDD8);
const darkModeTextColor = Color(0xFFECECEC);
const darkModetabTextColor = Color(0xFF9E9AB5);
const darkModeTextSecondaryColor = Color(0xFF555172);
const darkHintColor = Color(0xFF7C7D8E);

class KosoTheme {
  static ThemeData light() {
    // before we return the theme
    // we need to ensure that system ui chrome
    // setted
    if (!Get.isPlatformDarkMode) {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(
          systemNavigationBarColor: primaryColor,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.light,
          statusBarColor: Colors.transparent,
        ),
      );
    }

    // theme data
    // all of teh theme
    return ThemeData(
      brightness: Brightness.light,
      primaryColor: primaryColor,
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      hintColor: primaryColor,
      focusColor: primaryColor,
      dividerColor: Colors.grey[200],
      primaryColorDark: primaryColor,
      colorScheme: const ColorScheme.light(
        primary: primaryColor,
        error: Colors.red,
      ),
      fontFamily: GoogleFonts.ibmPlexSans().fontFamily,
      appBarTheme: AppBarTheme(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        shadowColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: primaryColor,
          size: 28.0,
        ),
        titleTextStyle: TextStyle(
          fontSize: 16.0,
          color: primaryColor,
          fontFamily: GoogleFonts.ibmPlexSans().fontFamily,
          fontWeight: FontWeight.w700,
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          foregroundColor:
              MaterialStateProperty.resolveWith((states) => primaryColor),
          side: MaterialStateProperty.resolveWith(
            (states) => const BorderSide(
              width: 2.0,
              color: primaryColor,
            ),
          ),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.ibmPlexSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => primaryColor.withOpacity(0.3)),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor:
              MaterialStateProperty.resolveWith((states) => primaryColor),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.ibmPlexSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          foregroundColor:
              MaterialStateProperty.resolveWith((states) => textColor),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => const Color(0xFF0A0913)),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.ibmPlexSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => const Color(0xFF0A0913).withOpacity(0.4)),
        ),
      ),
      dialogTheme: DialogTheme(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
        elevation: 0.0,
        titleTextStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w700,
          fontSize: 18.0,
        ),
        contentTextStyle: GoogleFonts.ibmPlexSans().copyWith(
          height: 1.4,
        ),
      ),
      chipTheme: ChipThemeData(
        backgroundColor: primaryColor,
        disabledColor: Colors.grey[300]!,
        selectedColor: Colors.white,
        secondarySelectedColor: Colors.white30,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
        labelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        secondaryLabelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        brightness: Brightness.light,
      ),
      iconTheme: const IconThemeData(
        size: 20.0,
        color: primaryColor,
      ),
      radioTheme: RadioThemeData(
        fillColor: MaterialStateProperty.resolveWith((states) => primaryColor),
      ),
      tabBarTheme: TabBarTheme(
        labelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        unselectedLabelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        indicator: BoxDecoration(
          color: primaryColor,
          borderRadius: BorderRadius.circular(16.0),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: primaryColor,
      ),
      textTheme: TextTheme(
        headline3: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
        headline5: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
          fontWeight: FontWeight.bold,
        ),
        headline6: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
        subtitle1: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
        bodyText1: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
        bodyText2: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
        button: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
        caption: GoogleFonts.ibmPlexSans().copyWith(
          color: primaryColor,
        ),
      ),
    );
  }

  static ThemeData dark() {
    // before we return the theme
    // we need to ensure that system ui chrome
    // setted
    if (Get.isPlatformDarkMode) {
      SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(
          systemNavigationBarColor: darkTextColor,
          statusBarIconBrightness: Brightness.light,
          systemNavigationBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.light,
          statusBarColor: Colors.transparent,
        ),
      );
    }

    // theme data
    // all of teh theme
    return ThemeData(
      brightness: Brightness.dark,
      primaryColor: darkModePrimaryColor,
      backgroundColor: darkTextColor,
      scaffoldBackgroundColor: darkModeBackgroundColor,
      dividerColor: darkModePrimaryColor,
      hintColor: darkHintColor,
      focusColor: darkModePrimaryColor,
      primaryColorDark: primaryColor,
      colorScheme: const ColorScheme.dark(
        primary: primaryColor,
        error: Colors.red,
      ),
      fontFamily: GoogleFonts.ibmPlexSans().fontFamily,
      appBarTheme: AppBarTheme(
        backgroundColor: darkModeBackgroundColor,
        elevation: 0.0,
        centerTitle: true,
        shadowColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: darkTextColor,
          size: 28.0,
        ),
        titleTextStyle: TextStyle(
          fontSize: 16.0,
          color: darkTextColor,
          fontFamily: GoogleFonts.ibmPlexSans().fontFamily,
          fontWeight: FontWeight.w700,
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          foregroundColor:
              MaterialStateProperty.resolveWith((states) => darkTextColor),
          side: MaterialStateProperty.resolveWith(
            (states) => const BorderSide(
              width: 2.0,
              color: darkModePrimaryColor,
            ),
          ),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.ibmPlexSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => primaryColor.withOpacity(0.3)),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) => darkModePrimaryColor,
          ),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.ibmPlexSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          foregroundColor:
              MaterialStateProperty.resolveWith((states) => darkTextColor),
          elevation: MaterialStateProperty.resolveWith((states) => 0),
          fixedSize: MaterialStateProperty.resolveWith(
            (states) => const Size.fromHeight(52.0),
          ),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
            (states) => darkTextColor,
          ),
          textStyle: MaterialStateProperty.resolveWith(
            (states) => GoogleFonts.ibmPlexSans().copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          overlayColor: MaterialStateProperty.resolveWith(
              (states) => const Color(0xFF0A0913).withOpacity(0.4)),
        ),
      ),
      chipTheme: ChipThemeData(
        backgroundColor: darkModePrimaryColor,
        disabledColor: Colors.grey[300]!,
        selectedColor: darkTextColor,
        secondarySelectedColor: Colors.white30,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
        labelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        secondaryLabelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
        ),
        brightness: Brightness.light,
      ),
      iconTheme: const IconThemeData(
        size: 20.0,
        color: darkTextColor,
      ),
      radioTheme: RadioThemeData(
        fillColor: MaterialStateProperty.resolveWith((states) => primaryColor),
      ),
      dialogTheme: DialogTheme(
        backgroundColor: darkModePrimaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
        elevation: 0.0,
        titleTextStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w700,
          fontSize: 18.0,
        ),
        contentTextStyle: GoogleFonts.ibmPlexSans().copyWith(
          height: 1.4,
        ),
      ),
      tabBarTheme: TabBarTheme(
        labelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        unselectedLabelStyle: GoogleFonts.ibmPlexSans().copyWith(
          fontWeight: FontWeight.w600,
          fontSize: 13.0,
        ),
        indicator: BoxDecoration(
          color: darkModePrimaryColor,
          borderRadius: BorderRadius.circular(16.0),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: darkTextColor,
      ),
      textTheme: TextTheme(
        headline3: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
        headline5: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
          fontWeight: FontWeight.bold,
        ),
        headline6: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
        subtitle1: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
        bodyText1: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
        bodyText2: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
        button: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
        caption: GoogleFonts.ibmPlexSans().copyWith(
          color: darkTextColor,
        ),
      ),
    );
  }
}
