import 'package:flutter/material.dart';

const intros = [
  {
    "image": "assets/images/e-commerce.svg",
    "title": "We Develop Your Activity",
    "desc": "Access your freedom, manage all of your task in one place "
  },
  {
    "image": "assets/images/loading.svg",
    "title": "Make Your Life Easier",
    "desc": "Improve your productivity, and knowing what you want"
  },
  {
    "image": "assets/images/music-celebrate.svg",
    "title": "Save the idea",
    "desc": "Dont know, what you want todo. Just save it"
  },
  {
    "image": "assets/images/travels.svg",
    "title": "Knowing Your Self",
    "desc": "Prepare everything before we start"
  },
];

var generalNotificationChannelKey = "koso_general_channel";

var favoritedColors = [
  {"code": "17D99F", "color": const Color(0xFF17D99F)},
  {"code": "177CD9", "color": const Color(0xFF177CD9)},
  {"code": "2717D9", "color": const Color(0xFF2717D9)},
  {"code": "D917BA", "color": const Color(0xFFD917BA)},
  {"code": "47DAEE", "color": const Color(0xFF47DAEE)},
  {"code": "D6666D", "color": const Color(0xFFD6666D)},
];

// define the supported locales,
// we need to specify this for the system translation and locale
var localesSupported = const [
  Locale("en", "Us"),
  Locale("id", "ID"),
];
