import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koso/configs/translation.dart';
import 'package:koso/themes/theme.dart';
import 'package:koso/views/splash_view.dart';

class KosoApp extends StatelessWidget {
  const KosoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "Koso",
      home: const SplashView(),
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      // locale: Get.deviceLocale,
      locale: const Locale("id", "ID"),
      fallbackLocale: const Locale("en", "US"),
      translations: KosoTranslation(),
      theme: KosoTheme.light(),
      darkTheme: KosoTheme.dark(),
      enableLog: true,
    );
  }
}
