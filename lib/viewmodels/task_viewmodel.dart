import 'package:get/get.dart';
import 'package:koso/components/alert.dart';
import 'package:koso/models/task_model.dart';
import 'package:koso/services/task_service.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';
import 'package:koso/viewmodels/notification_viewmodel.dart';

class TaskViewModel extends GetxController {
  // variables
  List<TaskModel> tasks = [];

  // bind viewmodel
  final AccountViewModel _accountViewModel = Get.find();

  // bind services
  final TaskService _taskService = TaskService();

  @override
  void onReady() async {
    super.onReady();

    // load the task
    await loadAllTask();

    // run schedule task
    Get.find<NotificationViewModel>().scheduleTaskNotification(tasks);
  }

  // load the task by the owner
  Future<void> loadAllTask() async {
    // we only can doing this if
    // the account already exist
    // so we need to check it first
    if (_accountViewModel.account.id != null) {
      // load the task
      tasks =
          await _taskService.readAllTaskByOwner(_accountViewModel.account.id!);
    }
    update();
  }

  List<TaskModel> filterTask(String conditionName, {String? query}) {
    // define the variables
    List<TaskModel> conditionedTasks = [];

    // we need to filter the action we are going todo
    // we need to filter by its status and deadline for today, upcoming, pending, all and done
    // all task
    if (conditionName.toLowerCase() == "all") {
      conditionedTasks = tasks;
    }

    // today list of task
    if (conditionName.toLowerCase() == "today") {
      conditionedTasks = tasks
          .where((element) =>
              element.deadline != null &&
              element.deadline!.day.isEqual(DateTime.now().day) &&
              element.status != null &&
              element.status!.isNotEmpty &&
              element.status != "done")
          .toList();
    }

    // the upcoming task
    // list all task for upcoming task
    if (conditionName.toLowerCase() == "upcoming") {
      conditionedTasks = tasks
          .where((element) =>
              element.deadline != null &&
              element.deadline!.isAfter(DateTime.now()) &&
              element.status != null &&
              element.status!.isNotEmpty &&
              element.status != "done")
          .toList();
    }

    // pending task
    // list all task that pending, and not finished yet
    if (conditionName.toLowerCase() == "pending") {
      conditionedTasks = tasks
          .where((element) =>
              element.deadline != null &&
              element.deadline!.isBefore(DateTime.now()) &&
              element.status != null &&
              element.status!.isNotEmpty &&
              element.status != "done")
          .toList();
    }

    // doned task
    // list all done tasks
    if (conditionName.toLowerCase() == "done") {
      conditionedTasks = tasks
          .where(
              (element) => element.status != null && element.status == "done")
          .toList();
    }

    // filter by the title, or query the user
    if (query != null && query.isNotEmpty) {
      conditionedTasks = conditionedTasks
          .where((element) =>
              element.title != null &&
              element.title!.toLowerCase().contains(query.toLowerCase()))
          .toList();
    }
    return conditionedTasks;
  }

  // count the finished task
  // the task done identify by done status
  int countFinishedTask() {
    int count = 0;

    // count the task
    count = tasks
        .where((element) => element.status != null && element.status == "done")
        .length;

    return count;
  }

  // count pending task
  // mean that task overdue
  // this can be the over dealine task and hold
  int countPendingTask() {
    int count = 0;

    // count task
    count = tasks
        .where((element) =>
            element.deadline != null &&
            element.deadline!.isBefore(DateTime.now()) &&
            element.status != null &&
            element.status != "done")
        .length;

    return count;
  }

  // count inprogress task
  // the in progress task, mean the task today
  int countInProgressTask() {
    int count = 0;

    // coun the task
    count = tasks
        .where((element) =>
            element.deadline != null &&
            element.deadline!.day.isEqual(DateTime.now().day) &&
            element.status != null &&
            element.status != "done")
        .length;

    return count;
  }

  Future<void> saveTask(TaskModel newTask) async {
    // after the task added, we
    // need to save this task into database too
    // when this start, we need to show the notice alert dialog
    Get.dialog(const SaveDataAlert());

    var data = TaskModel();

    // we need to split the update and create task
    if (newTask.id != null) {
      // update
      data = await _taskService.updateTask(newTask);
    } else {
      // we create one
      data = await _taskService.addTask(newTask);

      // after finish create the task, we are going to show notification
      // when the task finished
      if (data.id != null) {
        Get.find<NotificationViewModel>().createNotificationForNewTask(data);
      }
    }

    if (Get.isDialogOpen!) {
      await Future.delayed(const Duration(milliseconds: 600));
      Get.back();
    }

    // we only add this if, data not empty or null
    if (data.id != null) {
      await loadAllTask();

      // after finish to add
      // we are going to go bac again
      Get.back();
    } else {
      // we gonna show the error saved task
      Get.dialog(const FailedSaveDataAlert());
    }

    update();
  }

  // delete task
  Future<void> deleteTask(TaskModel task) async {
    // we are going to delete the task in cloud
    // we go
    _taskService.deleteTask(task);

    // we need to delete the task immediatly
    tasks.remove(task);

    // update the widget
    update();
  }

  // finish task
  Future<void> finishTask(TaskModel task) async {
    // we need to done the task in the cloud
    // before we go we need to update the task by their status
    task.status = "done";

    _taskService.updateTask(task);

    // we need to update the task

    update();
  }
}
