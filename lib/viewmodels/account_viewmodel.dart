// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:koso/components/alert.dart';
// import 'package:google_sign_in/google_sign_in.dart';
import 'package:koso/models/account_model.dart';
import 'package:koso/services/account_service.dart';
import 'package:koso/services/auth_service.dart';
import 'package:koso/services/storage_service.dart';
import 'package:koso/utils/functions.dart';
import 'package:koso/viewmodels/task_viewmodel.dart';
import 'package:koso/views/home_view.dart';
import 'package:koso/views/login_view.dart';

class AccountViewModel extends GetxController {
  // variables
  late AccountModel account = AccountModel();
  late User? user;

  // bind service
  final AccountService _accountService = AccountService();
  final AuthService _authService = AuthService();
  final StorageService _storageService = StorageService();

  @override
  void onReady() async {
    super.onReady();

    // load the account
    await loadAccount();
  }

  // load the account data
  //
  Future<void> loadAccount() async {
    user = await _authService.currentUser();
    if (user != null) {
      account = await _accountService.currentAccount(user!.email!);
    }
  }

  // sign with google
  Future<void> signInWithGoogle() async {
    // we need to get the credential of user login
    user = await _authService.signinWithGoogle();

    // now if the credential not empty
    // we will check the current user data and update it
    if (user != null) {
      // ok now we need to show the user about notice, as ui
      // that we need to process their acount data
      Get.dialog(const SignAlert());

      account = await _accountService.currentAccount(user!.email!);

      // we need to create one record of user
      // if not exist
      if (account.id == null) {
        var newAccount = AccountModel(
          avatar: user!.photoURL,
          email: user!.email,
          fullName: user!.displayName,
          bio: "",
        );

        account = await _accountService.createAccount(newAccount);
      }

      // we need to close the dialog
      // use state for notice sign in
      if (Get.isDialogOpen!) {
        Get.back();
        await Future.delayed(const Duration(milliseconds: 600));
      }

      // check the account
      // is user already registered and sign in into apps
      if (user != null && account.id != null) {
        // load the task first
        Get.find<TaskViewModel>().loadAllTask();

        // the user already sign in
        // we need to going into home
        Get.offAll(() => const HomeView());
      } else {
        // opps, the user failed to sign in
        // we need to show dialog again, again.
        Get.dialog(const FailedSignAlert());
      }
    }

    update();
  }

  // is sign in method
  Future<bool> isSignIn() async {
    var status = false;
    if (user != null) {
      status = true;
    }
    return status;
  }

  // logout from this app
  // enable user to out from this app
  Future<void> signOut() async {
    account = AccountModel();
    user = null;

    // end session
    await _authService.endSession();

    // ok after finish to clear all cache and session,
    // we need to bring the user into login screen
    Get.offAll(() => const LoginView());
    update();
  }

  // update avatar
  Future<void> updateAvatar() async {
    // to update the avatar, we need to pick some image, that we can load
    // and up into storage

    // the first we need to do, is show the source of image we want to get
    var file = await pickImageSource();

    // check the file, ensure its not empty or null
    if (file != null) {
      // the file exists
      // we need to define the path, and upload into the storage
      var path = "account/avatar/${account.id.toString()}_avatar.png";
      var url =
          await _storageService.uploadImageData(path, await file.readAsBytes());

      // we need to check the url
      if (url != null) {
        // ok, its complete, we need to update the account of avatar
        account.avatar = url;
        account = await _accountService.updateAccount(account);

        update();
      }
    }
  }

  // update Account
  Future<void> updateAccount(AccountModel newAccount) async {
    // when update, start
    // we need to show the save dialog into user, so the user can notice the proccess
    Get.dialog(const SaveDataAlert());

    // save the data, start execute
    var data = await _accountService.updateAccount(newAccount);

    // close the current state
    if (Get.isDialogOpen!) {
      Get.back();
      await Future.delayed(const Duration(milliseconds: 600));
    }

    // after finish we need to check the
    // data, is this already saved
    if (data.id != null) {
      account = data;
      // data saved,
      // back
      Get.back();
    } else {
      // save failed
      Get.dialog(const FailedSaveDataAlert());
    }

    update();
  }
}
