import 'dart:convert';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:koso/data/data.dart';
import 'package:koso/models/notification_model.dart';
import 'package:koso/models/task_model.dart';
import 'package:koso/themes/theme.dart';
import 'package:koso/views/home_view.dart';
import 'package:koso/views/notification_view.dart';
import 'package:koso/views/task_view.dart';

class NotificationViewModel extends GetxController {
  // variables
  late List<NotificationDetailModel> notifications = [];

  // bind viewmodel

  @override
  void onReady() {
    super.onReady();

    initializeNotification();
    requestNotificationPermision();
    listenActionNotification();
    initializeNotificationService();
  }

  // initialize the notification channel
  void initializeNotification() {
    AwesomeNotifications().initialize(
      null,
      [
        // specify channel,
        // the general channel
        NotificationChannel(
          channelKey: generalNotificationChannelKey,
          channelName: 'Koso General notification',
          channelDescription: 'Notification channel for Koso',
          importance: NotificationImportance.High,
          defaultColor: primaryColor,
          channelShowBadge: true,
          ledColor: Colors.white,
        ),
      ],
    );
  }

  // request permision, of notification
  void requestNotificationPermision() async {
    // first we gonna do, is check the notification permision
    bool allowShowNotification =
        await AwesomeNotifications().isNotificationAllowed();

    if (!allowShowNotification) {
      // we need to define the permision list of the
      // notification going to use

      // define permision we need
      var permisions = [
        // define the whole permision going to allowed
        NotificationPermission.Alert,
        NotificationPermission.Badge,
        NotificationPermission.Car,
        NotificationPermission.Sound,
        NotificationPermission.CriticalAlert,
        NotificationPermission.Provisional,
      ];

      // Check which of the permissions you need are allowed at this time
      List<NotificationPermission> permissionsAllowed =
          await AwesomeNotifications().checkPermissionList(
        permissions: permisions,
      );

      // check the permision allowed, and if there one permision
      // not granted we need to request permision
      if (permissionsAllowed.length < permisions.length) {
        // we need to grand access for this notification permision

        // get the permision needed, for the notification
        // refresh the permision we needed, only get we needed
        List<NotificationPermission> permissionsNeeded =
            permisions.toSet().difference(permissionsAllowed.toSet()).toList();

        if (permissionsNeeded.isNotEmpty) {
          // we need to gain access again
          // Request the permission through native resources.
          await AwesomeNotifications().requestPermissionToSendNotifications(
            permissions: permissionsNeeded,
          );

          // After the user come back, check if the permissions has successfully enabled
          permissionsAllowed = await AwesomeNotifications().checkPermissionList(
            permissions: permissionsNeeded,
          );
        }
      }
    }
  }

  // listen, stream, the notification action
  void listenActionNotification() {
    // when a new notification going to created
    AwesomeNotifications().createdStream.listen((receiveNotification) {});

    // when the new notification going to display
    AwesomeNotifications().displayedStream.listen((receiveNotification) {
      // we need to add the notification in to list of notification detail
      // we need to get the payload
      var payload = receiveNotification.payload;
      var topic = "general";

      // we need to specify, set the topic with data we passed
      if (payload != null) {
        if (payload["topic"] != null) {
          topic = payload["topic"]!;
        }
      }

      // ok after we specify the topic, and the payload
      // we need to take action, where is this notification going, what data, what action
      if (topic == "general") {
        // general notification
        // when the general notification, we need
        // to bring it into notification view
        // ! Not Implement Yet
      } else if (topic == "task") {
        // task notification
        // when the task, we are going to go into task view
        var decodedTask = json.decode(payload!["data"]!);
        var task = TaskModel.fromMap(decodedTask["id"] ?? "", decodedTask);

        var dataPayload = task.toMap();
        // add the id, we need to specify the id
        dataPayload.putIfAbsent("id", () => task.id);

        var notif = NotificationDetailModel(
          topic: "task",
          title: "Task Updated",
          summary: "Check your task",
          description: task.title,
          date: task.deadline,
          data: dataPayload,
        );

        notifications.add(notif);
      } else if (topic == "promo") {
        // promo notification
        // the promo
        // ! Not implemnt yet
      } else if (topic == "content") {
        // content notification
        // the content notification
        // ! Not implement yet
      } else {
        // the other one
        // the major notification, we need going into Home View

      }
    });

    // when the user tap, or take action for the notification
    AwesomeNotifications().actionStream.listen((receiveAction) {
      // we need going into notification page
      // if the notification get action from user

      // we need to identify the type, or topic of the notification
      var payload = receiveAction.payload;
      var topic = "general";

      // we need to specify, set the topic with data we passed
      if (payload != null) {
        if (payload["topic"] != null) {
          topic = payload["topic"]!;
        }
      }

      // ok after we specify the topic, and the payload
      // we need to take action, where is this notification going, what data, what action
      if (topic == "general") {
        // general notification
        // when the general notification, we need
        // to bring it into notification view
        Get.to(() => const NotitficationView());
      } else if (topic == "task") {
        // task notification
        // when the task, we are going to go into task view
        var decodedTask = json.decode(payload!["data"]!);
        var task = TaskModel.fromMap(decodedTask["id"] ?? "", decodedTask);

        Get.to(
          () => const TaskView(),
          arguments: {
            "task": task,
          },
        );
      } else if (topic == "promo") {
        // promo notification
        // the promo
        // ! Not implemnt yet
      } else if (topic == "content") {
        // content notification
        // the content notification
        // ! Not implement yet
      } else {
        // the other one
        // the major notification, we need going into Home View
        Get.offAll(() => const HomeView());
      }
    });

    // when user, close, dismis for serveral notification
    AwesomeNotifications().dismissedStream.listen((receiveAction) {
      // when user dismis the notification
      // nothing todo
      // ? We do nothing ?
    });
  }

  // initialize the notification service like firebase, one signal, pulse,
  // and so on
  void initializeNotificationService() {
    // firebase
    FirebaseMessaging.onBackgroundMessage((message) async {
      AwesomeNotifications().createNotificationFromJsonData(message.data);
    });

    // one signal, not implement yet
  }

  // scedule task notification
  // allow to scedule the notification from task,
  // that user create with deadline and start
  void scheduleTaskNotification(List<TaskModel> tasks) async {
    // handle the scedule notification, for task

    // we define the notification for whole task
    // ok, we are go
    for (var task in tasks) {
      var dataPayload = task.toMap();
      // add the id, we need to specify the id
      dataPayload.putIfAbsent("id", () => task.id);

      AwesomeNotifications().createNotification(
        content: NotificationContent(
          id: task.deadline!.millisecond,
          channelKey: generalNotificationChannelKey,
          title: 'Your Task May Overdue',
          body: 'Checkout your task when overdue',
          summary: "Please Complete this task",
          displayOnBackground: true,
          displayOnForeground: true,
          payload: {
            "topic": "task",
            "data": json.encode(dataPayload),
          },
          hideLargeIconOnExpand: true,
          wakeUpScreen: true,
          notificationLayout: NotificationLayout.Default,
        ),
        schedule: NotificationCalendar.fromDate(date: task.deadline!),
      );
    }
  }

  // create notification for new task
  Future<void> createNotificationForNewTask(TaskModel task) async {
    // the first we gonna do is, convert this task into notification
    // create payload data
    var dataPayload = task.toMap();
    // add the id, we need to specify the id
    dataPayload.putIfAbsent("id", () => task.id);

    var notif = NotificationDetailModel(
      topic: "task",
      title: "Task Created",
      summary: "New Task was created",
      description: task.title,
      date: task.deadline,
      data: dataPayload,
    );

    // the we need to add this notif into list of notification we have
    notifications.add(notif);

    // now create a new notification
    await AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: task.deadline!.millisecond,
        channelKey: generalNotificationChannelKey,
        title: notif.title,
        body: notif.description,
        summary: notif.summary,
        displayOnBackground: true,
        displayOnForeground: true,
        payload: {
          "topic": "task",
          "data": json.encode(notif.data),
        },
        hideLargeIconOnExpand: true,
        wakeUpScreen: true,
        notificationLayout: NotificationLayout.Default,
      ),
    );

    update();
  }

  // today notification list
  List<NotificationDetailModel> todayNotificationList() {
    var todayNotifs = [];

    // now we gonna, filter the notification for today
    // base on on deadline
    todayNotifs = notifications
        .where((element) =>
            element.data != null && element.date!.day == DateTime.now().day)
        .toList();

    return todayNotifs as List<NotificationDetailModel>;
  }

  // week notification lists
  List<NotificationDetailModel> weekNotificationList() {
    var weekNotifs = [];

    // now we gonna, filter the notification for today
    // base on on deadline
    weekNotifs = notifications
        .where((element) =>
            element.data != null &&
            element.date!.weekday == DateTime.now().weekday)
        .toList();

    return weekNotifs as List<NotificationDetailModel>;
  }

  // remove notification
  void removeNotification(NotificationDetailModel notif) {
    notifications.remove(notif);

    // update the ui
    update();
  }
}
