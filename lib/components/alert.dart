import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:lottie/lottie.dart';

class SignAlert extends StatelessWidget {
  const SignAlert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we have some content here
          // depend on the sign situation
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color: Get.theme.dialogTheme.backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Get.theme.primaryColor.withOpacity(0.3),
                    blurRadius: 40.0,
                    spreadRadius: -2.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  // we have some animation for
                  // show into user that we are still signin
                  SizedBox(
                    height: 120.0,
                    child: LottieBuilder.network(
                      "https://assets9.lottiefiles.com/packages/lf20_iae48o38.json",
                      fit: BoxFit.contain,
                    ),
                  ),

                  // we have some title for the alert
                  const SizedBox(height: 24.0),
                  Text(
                    "sign_alert_title".tr,
                    style: Get.theme.dialogTheme.titleTextStyle,
                  ),

                  // description
                  const SizedBox(height: 24.0),
                  Text(
                    "sign_alert_desc".tr,
                    style: Get.theme.dialogTheme.contentTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FailedSignAlert extends StatelessWidget {
  const FailedSignAlert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we need to have the close
          // dismisable
          // enable user to close the dialog
          GestureDetector(
            onTap: () {
              Get.back();
            },
          ),

          // we have some content here
          // depend on the sign situation
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color: Get.theme.dialogTheme.backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Get.theme.primaryColor.withOpacity(0.3),
                    blurRadius: 40.0,
                    spreadRadius: -2.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  // we have some animation for
                  // show into user that we are still signin
                  SizedBox(
                    height: 120.0,
                    child: LottieBuilder.network(
                      "https://assets9.lottiefiles.com/packages/lf20_iae48o38.json",
                      fit: BoxFit.contain,
                    ),
                  ),

                  // we have some title for the alert
                  const SizedBox(height: 24.0),
                  Text(
                    "failed_alert_title".tr,
                    style: Get.theme.dialogTheme.titleTextStyle,
                  ),

                  // description
                  const SizedBox(height: 24.0),
                  Text(
                    "failed_alert_desc".tr,
                    style: Get.theme.dialogTheme.contentTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class NotImplementFeatureAlert extends StatelessWidget {
  const NotImplementFeatureAlert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we need to have the close
          // dismisable
          // enable user to close the dialog
          GestureDetector(
            onTap: () {
              Get.back();
            },
          ),

          // we have some content here
          // depend on the sign situation
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color: Get.theme.dialogTheme.backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Get.theme.primaryColor.withOpacity(0.3),
                    blurRadius: 40.0,
                    spreadRadius: -2.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  // we have some animation for
                  // show into user that we are still signin
                  Container(
                    height: 72.0,
                    width: 72.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Get.theme.primaryColorDark,
                    ),
                    child: Icon(
                      IconlyBold.game,
                      size: 32.0,
                      color: !Get.isDarkMode ? Colors.white : null,
                    ),
                  ),

                  // we have some title for the alert
                  const SizedBox(height: 24.0),
                  Text(
                    "not_implement_alert_title".tr,
                    style: Get.theme.dialogTheme.titleTextStyle,
                  ),

                  // description
                  const SizedBox(height: 24.0),
                  Text(
                    "not_implement_alert_desc".tr,
                    style: Get.theme.dialogTheme.contentTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ImagePickSourceAlert extends StatelessWidget {
  const ImagePickSourceAlert({
    Key? key,
    this.onCamera,
    this.onGalery,
  }) : super(key: key);
  final Function? onCamera;
  final Function? onGalery;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we need to have the close
          // dismisable
          // enable user to close the dialog
          GestureDetector(
            onTap: () {
              Get.back();
            },
          ),

          // we have some content here
          // depend on the sign situation
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color: Get.theme.dialogTheme.backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Get.theme.primaryColor.withOpacity(0.3),
                    blurRadius: 40.0,
                    spreadRadius: -2.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  // we have some animation for
                  // show into user that we are still signin
                  Container(
                    height: 72.0,
                    width: 72.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Get.theme.primaryColorDark,
                    ),
                    child: Icon(
                      IconlyBold.image,
                      size: 32.0,
                      color: !Get.isDarkMode ? Colors.white : null,
                    ),
                  ),

                  // we have some title for the alert
                  const SizedBox(height: 24.0),
                  Text(
                    "image_source_alert_title".tr,
                    style: Get.theme.dialogTheme.titleTextStyle,
                  ),

                  // description
                  const SizedBox(height: 24.0),
                  Text(
                    "image_source_alert_desc".tr,
                    style: Get.theme.dialogTheme.contentTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20.0),

                  SizedBox(
                    width: Get.width,
                    child: OutlinedButton(
                      onPressed: () {
                        if (onCamera != null) {
                          onCamera!();
                        }
                      },
                      child: Text("image_source_alert_camera_button_label".tr),
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  SizedBox(
                    width: Get.width,
                    child: OutlinedButton(
                      onPressed: () {
                        if (onGalery != null) {
                          onGalery!();
                        }
                      },
                      child: Text("image_source_alert_galery_button_label".tr),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SaveDataAlert extends StatelessWidget {
  const SaveDataAlert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we have some content here
          // depend on the sign situation
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color: Get.theme.dialogTheme.backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Get.theme.primaryColor.withOpacity(0.3),
                    blurRadius: 40.0,
                    spreadRadius: -2.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  // we have some animation for
                  // show into user that we are still signin
                  Container(
                    height: 72.0,
                    width: 72.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Get.theme.primaryColorDark,
                    ),
                    child: Icon(
                      IconlyBold.paper_upload,
                      size: 32.0,
                      color: !Get.isDarkMode ? Colors.white : null,
                    ),
                  ),

                  // we have some title for the alert
                  const SizedBox(height: 24.0),
                  Text(
                    "save_data_alert_title".tr,
                    style: Get.theme.dialogTheme.titleTextStyle,
                  ),

                  // description
                  const SizedBox(height: 24.0),
                  Text(
                    "save_data_alert_desc".tr,
                    style: Get.theme.dialogTheme.contentTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FailedSaveDataAlert extends StatelessWidget {
  const FailedSaveDataAlert({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we need to have the close
          // dismisable
          // enable user to close the dialog
          GestureDetector(
            onTap: () {
              Get.back();
            },
          ),

          // we have some content here
          // depend on the sign situation
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32.0),
                color: Get.theme.dialogTheme.backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Get.theme.primaryColor.withOpacity(0.3),
                    blurRadius: 40.0,
                    spreadRadius: -2.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  // we have some animation for
                  // show into user that we are still signin
                  Container(
                    height: 72.0,
                    width: 72.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Get.theme.primaryColorDark,
                    ),
                    child: Icon(
                      IconlyBold.image,
                      size: 32.0,
                      color: !Get.isDarkMode ? Colors.white : null,
                    ),
                  ),

                  // we have some title for the alert
                  const SizedBox(height: 24.0),
                  Text(
                    "failed_save_data_alert_title".tr,
                    style: Get.theme.dialogTheme.titleTextStyle,
                  ),

                  // description
                  const SizedBox(height: 24.0),
                  Text(
                    "failed_save_data_alert_desc".tr,
                    style: Get.theme.dialogTheme.contentTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
