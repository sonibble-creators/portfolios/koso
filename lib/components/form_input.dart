import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:koso/components/date_picker.dart';

class FormInput extends StatefulWidget {
  final EdgeInsets? margin;
  final String? label;
  final TextInputType? inputType;
  final Function(dynamic value)? onSaved;
  final Function(dynamic value)? onSubmited;
  final TextEditingController? controller;
  final String? initial;
  final String? hint;
  final bool? enable;

  const FormInput({
    Key? key,
    this.margin = EdgeInsets.zero,
    this.label,
    this.inputType = TextInputType.text,
    this.onSaved,
    this.initial,
    this.hint,
    this.enable = true,
    this.onSubmited,
    this.controller,
  }) : super(key: key);

  @override
  _FormInputState createState() => _FormInputState();
}

class _FormInputState extends State<FormInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // label
          if (widget.label != null && widget.label!.isNotEmpty) ...[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                widget.label!,
                style: Get.textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const SizedBox(height: 8.0),
          ],

          // main form
          TextFormField(
            key: widget.key,
            style: Get.textTheme.bodyText1,
            initialValue: widget.initial,
            onFieldSubmitted: (value) {
              if (widget.onSubmited != null) {
                widget.onSubmited!(value);
              }
            },
            controller: widget.controller,
            onSaved: (newValue) {
              if (widget.onSaved != null) {
                widget.onSaved!(newValue);
              }
            },
            cursorColor: Get.theme.hintColor,
            enabled: widget.enable,
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: Get.textTheme.bodyText2!.copyWith(
                color: Get.theme.hintColor,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Get.theme.dividerColor,
                  width: 1,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Get.theme.dividerColor,
                  width: 1,
                ),
              ),
              focusColor: Get.theme.focusColor,
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Get.theme.dividerColor,
                  width: 1.4,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DateFormInput extends StatefulWidget {
  final EdgeInsets? margin;
  final String? label;
  final TextInputType? inputType;
  final Function(DateTime? value)? onSaved;
  final DateTime? initial;
  final DateTime? firstDate;
  final DateTime? lastDate;
  final String? hint;

  const DateFormInput({
    Key? key,
    this.margin = EdgeInsets.zero,
    this.label,
    this.inputType = TextInputType.text,
    this.onSaved,
    this.initial,
    this.hint,
    this.firstDate,
    this.lastDate,
  }) : super(key: key);

  @override
  _DateFormInputState createState() => _DateFormInputState();
}

class _DateFormInputState extends State<DateFormInput> {
  // variables
  late DateTime? _selectedDate;

  @override
  void initState() {
    super.initState();

    _selectedDate = widget.initial;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // label
          if (widget.label != null && widget.label!.isNotEmpty) ...[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                widget.label!,
                style: Get.textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const SizedBox(height: 8.0),
          ],

          // main form
          GestureDetector(
            onTap: () async {
              // we are going to pick the date
              _selectedDate = await showKosoDatePicker(
                initial: _selectedDate ?? DateTime.now(),
                firstDate:
                    widget.firstDate ?? DateTime(DateTime.now().year - 20),
                lastDate: widget.lastDate ?? DateTime(DateTime.now().year + 20),
              );

              // before we return, we need to update ui
              setState(() {});

              // return the date we picked
              widget.onSaved!(_selectedDate);
            },
            child: Container(
              constraints: const BoxConstraints(
                minHeight: 48.0,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                border: Border.all(color: Get.theme.dividerColor),
              ),
              child: Row(
                children: [
                  if (_selectedDate == null) ...[
                    Text(
                      widget.hint ?? "Pick a date",
                      style: Get.textTheme.bodyText2!.copyWith(
                        color: Get.theme.hintColor,
                      ),
                    ),
                  ] else ...[
                    Text(
                      DateFormat("EE, dd MMMM yyyy")
                          .format(_selectedDate ?? DateTime.now()),
                      style: Get.textTheme.bodyText1,
                    ),
                  ],
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class TextFormInput extends StatefulWidget {
  final EdgeInsets? margin;
  final String? label;
  final TextInputType? inputType;
  final Function(dynamic value)? onSaved;
  final String? initial;
  final String? hint;
  final int? maxLines;
  const TextFormInput({
    Key? key,
    this.margin,
    this.label,
    this.inputType,
    this.onSaved,
    this.initial,
    this.hint,
    this.maxLines = 5,
  }) : super(key: key);

  @override
  _TextFormInputState createState() => _TextFormInputState();
}

class _TextFormInputState extends State<TextFormInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // label
          if (widget.label != null && widget.label!.isNotEmpty) ...[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                widget.label!,
                style: Get.textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const SizedBox(height: 8.0),
          ],

          // main form
          TextFormField(
            key: widget.key,
            style: Get.textTheme.bodyText1,
            maxLines: widget.maxLines,
            initialValue: widget.initial,
            onSaved: (newValue) {
              if (widget.onSaved != null) {
                widget.onSaved!(newValue);
              }
            },
            cursorColor: Get.theme.hintColor,
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: Get.textTheme.bodyText2!.copyWith(
                color: Get.theme.hintColor,
              ),
              border: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Get.theme.dividerColor,
                  width: 1,
                ),
              ),
              focusColor: Get.theme.primaryColor,
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
                borderSide: BorderSide(
                  color: Get.theme.focusColor,
                  width: 1.4,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
