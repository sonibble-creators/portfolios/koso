import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

Future<DateTime?> showKosoDatePicker(
    {required DateTime initial,
    required DateTime firstDate,
    required DateTime lastDate}) async {
  var date = await Get.dialog<DateTime>(
    KosoDatePickerModal(
      initial: initial,
      firstDate: firstDate,
      lastDate: lastDate,
    ),
  );

  return date;
}

class KosoDatePickerModal extends StatefulWidget {
  final DateTime initial;
  final DateTime firstDate;
  final DateTime lastDate;

  const KosoDatePickerModal({
    Key? key,
    required this.initial,
    required this.firstDate,
    required this.lastDate,
  }) : super(key: key);

  @override
  _KosoDatePickerModalState createState() => _KosoDatePickerModalState();
}

class _KosoDatePickerModalState extends State<KosoDatePickerModal> {
  // define local variables
  late DateTime _selectedDate = DateTime.now();

  // widget variables
  late ScrollController _yearScrollController = ScrollController();
  late ScrollController _monthScrollController = ScrollController();
  late ScrollController _dateScrollController = ScrollController();

  // year picker
  Widget yearPickerWidget() => Container(
        height: 56.0,
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        child: ListView.builder(
          controller: _yearScrollController,
          itemCount: (widget.lastDate.year + 1 - widget.firstDate.year),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            // define the year
            // variables
            var year = widget.firstDate.year + index;

            return GestureDetector(
              onTap: () {
                // we need to set the selected date for year,
                // we are going to set the year of selected date
                _selectedDate =
                    DateTime(year, _selectedDate.month, _selectedDate.day);

                // when this year selected, we are going to put this item
                // into center
                _yearScrollController.animateTo(
                  index * 59.85 - (index ~/ 4),
                  duration: const Duration(milliseconds: 800),
                  curve: Curves.easeInOut,
                );

                // update the ui
                setState(() {});
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 6.0),
                  decoration: BoxDecoration(
                    color: _selectedDate.year == year
                        ? Get.theme.primaryColorDark
                        : Colors.transparent,
                    border: Border.all(
                      color: Get.theme.primaryColorDark,
                    ),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Text(
                    "$year",
                    style: Get.textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.bold,
                      color: _selectedDate.year == year ? Colors.white : null,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      );

  // month picker
  Widget monthPickerWidget() => Container(
        height: 56.0,
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        child: ListView.builder(
          itemCount: DateTime(_selectedDate.year, 1, 0).month,
          controller: _monthScrollController,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            // define the variables
            // month variables

            var month = index + 1;

            return Padding(
              padding: const EdgeInsets.only(right: 24.0),
              child: GestureDetector(
                onTap: () {
                  // we need to update the selected month, into the selected one
                  // we are going
                  _selectedDate =
                      DateTime(_selectedDate.year, month, _selectedDate.day);

                  // we need to bring this item into center
                  _monthScrollController.animateTo(
                    month * 40.87 - (month ~/ 2),
                    duration: const Duration(milliseconds: 800),
                    curve: Curves.easeInOut,
                  );

                  // update the ui
                  setState(() {});
                },
                child: Column(
                  children: [
                    // the text of mont
                    Text(
                      DateFormat("MMMM")
                          .format(DateTime(_selectedDate.year, month)),
                      style: _selectedDate.month == month
                          ? Get.textTheme.subtitle1!.copyWith(
                              fontWeight: FontWeight.bold,
                            )
                          : Get.textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                    ),

                    // we need an indicator for this
                    // the indicator use when the month are actived
                    if (_selectedDate.month == month) ...[
                      Container(
                        height: 4.0,
                        width: 4.0,
                        margin: const EdgeInsets.only(top: 4.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Get.isDarkMode
                              ? Colors.white54
                              : Get.theme.primaryColor,
                        ),
                      ),
                    ],
                  ],
                ),
              ),
            );
          },
        ),
      );

  // date picker
  Widget datePickerWidget() => SizedBox(
        height: 100.0,
        child: ListView.builder(
          controller: _dateScrollController,
          itemCount:
              DateTime(_selectedDate.year, _selectedDate.month + 1, 0).day,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            var day = index + 1;

            return GestureDetector(
              onTap: () {
                // need to change the selected date
                _selectedDate =
                    DateTime(_selectedDate.year, _selectedDate.month, day);

                // update selected date position, offset
                _dateScrollController.animateTo(
                  day * 59.85 - (day ~/ 4),
                  duration: const Duration(milliseconds: 800),
                  curve: Curves.easeInOut,
                );

                // we need to update ui
                setState(() {});
              },
              child: Container(
                width: 52.0,
                margin: const EdgeInsets.only(right: 16.0),
                decoration: BoxDecoration(
                  color: _selectedDate.day == day
                      ? Get.theme.primaryColorDark
                      : Colors.transparent,
                  border: Border.all(
                    color: Get.theme.primaryColorDark,
                  ),
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      DateFormat("EE").format(DateTime(
                          _selectedDate.year, _selectedDate.month, day)),
                      style: Get.textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                        color: _selectedDate.day == day ? Colors.white : null,
                      ),
                    ),
                    const SizedBox(height: 12.0),
                    Text(
                      DateFormat("d").format(DateTime(
                          _selectedDate.year, _selectedDate.month, day)),
                      style: Get.textTheme.subtitle1!.copyWith(
                        fontWeight: FontWeight.bold,
                        color: _selectedDate.day == day ? Colors.white : null,
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      );

  // time picker
  Widget timePickerWidget() => Container(
        height: 76.0,
        padding: const EdgeInsets.only(top: 24.0),
        child: ListView.builder(
          itemCount: 10,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Container(
              width: 56.0,
              margin: const EdgeInsets.only(right: 16.0),
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Center(
                child: Text(
                  "10:00",
                  style: Get.textTheme.subtitle1!.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            );
          },
        ),
      );

  // initialize
  void _initialize() {
    _selectedDate = widget.initial;

    // handle the scroll position, offset
    _yearScrollController = ScrollController(
      initialScrollOffset:
          (_selectedDate.year - widget.firstDate.year) * 59.85 -
              ((_selectedDate.year - widget.firstDate.year) ~/ 4),
    );
    _monthScrollController = ScrollController(
      initialScrollOffset:
          _selectedDate.month * 40.87 - (_selectedDate.month ~/ 2),
    );
    _dateScrollController = ScrollController(
      initialScrollOffset: _selectedDate.day * 59.85 - (_selectedDate.day ~/ 4),
    );
  }

  @override
  void initState() {
    super.initState();
    _initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          // we need to show the screen to back
          // dismisable screen
          GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Container(
              height: Get.height,
              width: Get.width,
              color: Colors.transparent,
            ),
          ),

          // the main content going here
          // start with year, month, day, and time
          Positioned(
            bottom: 20.0,
            left: 20.0,
            right: 20.0,
            child: Container(
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                color: Get.isDarkMode ? Get.theme.primaryColor : Colors.white,
                borderRadius: BorderRadius.circular(32.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // we need to show the picked date and time,
                  const SizedBox(height: 12.0),
                  Text(
                    DateFormat("EEEE, MMMM dd yyyy").format(_selectedDate),
                    style: Get.textTheme.subtitle1!.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  // year picker
                  const SizedBox(height: 20.0),
                  yearPickerWidget(),

                  // mont picker
                  monthPickerWidget(),

                  // date picker
                  datePickerWidget(),

                  // time picker
                  // timePickerWidget(),

                  // the confirm button
                  const SizedBox(height: 20.0),
                  Row(
                    children: [
                      const Spacer(),
                      ElevatedButton(
                        onPressed: () {
                          // when this pressed,
                          // we need to close the dialog
                          // and return the selected date
                          Get.back(result: _selectedDate);
                        },
                        child: const Text("Ok"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
