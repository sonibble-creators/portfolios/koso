import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:intl/intl.dart';
import 'package:koso/data/data.dart';
import 'package:koso/models/task_model.dart';
import 'package:koso/viewmodels/task_viewmodel.dart';
import 'package:koso/views/task_view.dart';

class TaskItemCard extends StatefulWidget {
  const TaskItemCard({
    Key? key,
    required this.task,
  }) : super(key: key);
  final TaskModel task;

  @override
  _TaskItemCardState createState() => _TaskItemCardState();
}

class _TaskItemCardState extends State<TaskItemCard> {
  // bind viewmodel
  final TaskViewModel _taskViewModel = Get.find();

  // deadline decoder
  String decodeDeadline(DateTime date) {
    String decodedDeadline = "";

    // day filter, date
    if (date.day == DateTime.now().day) {
      // same day
      decodedDeadline = "Today";
    } else if (date.day == DateTime.now().add(const Duration(days: 1)).day) {
      // this gonna be tomorow
      decodedDeadline = "Tomorrow";
    } else if (date.day == DateTime.now().day - 1) {
      // this gonna be
      decodedDeadline = "Yesterday";
    } else {
      // normal decoded
      decodedDeadline = DateFormat("EEEE, MM yyyy").format(date);
    }

    // time filter, time decoded
    decodedDeadline += "  " + DateFormat("jm").format(date);

    return decodedDeadline;
  }

  // decode color
  Color decodeColor(String colorCode) {
    var decodedColor = Colors.white;
    decodedColor = favoritedColors.singleWhere(
      (color) =>
          color["code"].toString().toLowerCase() == colorCode.toLowerCase(),
      orElse: () => favoritedColors.first,
    )["color"] as Color;

    // we need to modify the color, abour hue, saturate, etc.
    // specify for the theme
    // if (Get.isDarkMode) {
    //   // decodedColor = decodedColor.withGreen(75);
    // }

    return decodedColor;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // we going to edit or show the detail of this task
        Get.to(
          () => const TaskView(),
          arguments: {
            "task": widget.task.copyWith(),
          },
        );
      },
      child: Dismissible(
        key: ValueKey(widget.task),
        onDismissed: (direction) {
          // we are going to delete and mark the task as finish, before we again
          // we need to check the direction of this one
          if (direction == DismissDirection.endToStart) {
            // delete process
            _taskViewModel.deleteTask(widget.task);
          } else {
            // done process
            _taskViewModel.finishTask(widget.task);
          }
        },
        background: Row(
          children: [
            Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                color: Get.theme.primaryColor,
                shape: BoxShape.circle,
              ),
              child: Icon(
                IconlyLight.tick_square,
                size: 32.0,
                color: !Get.isDarkMode ? Colors.white : null,
              ),
            ),
          ],
        ),
        secondaryBackground: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                color: Get.theme.primaryColor,
                shape: BoxShape.circle,
              ),
              child: Icon(
                IconlyLight.delete,
                size: 32.0,
                color: !Get.isDarkMode ? Colors.white : null,
              ),
            ),
          ],
        ),
        child: Container(
          margin: const EdgeInsets.only(bottom: 20.0),
          padding: const EdgeInsets.fromLTRB(20, 16, 20, 20),
          decoration: BoxDecoration(
            color: decodeColor(widget.task.color!), // this will become dynamic
            borderRadius: BorderRadius.circular(24.0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // the title
              // title of the task
              Text(
                widget.task.title ?? "",
                style: Get.textTheme.headline6!.copyWith(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  height: 1.5,
                  color: Get.theme.primaryColorDark,
                ),
              ),

              // may we have an description for the task
              // the short description
              const SizedBox(height: 16.0),
              ConstrainedBox(
                constraints: const BoxConstraints(maxHeight: 39.0),
                child: Text(
                  widget.task.description ?? "",
                  style: Get.textTheme.bodyText2!.copyWith(
                    color: Get.theme.primaryColorDark,
                  ),
                ),
              ),

              // summary, tags, and complete button
              const SizedBox(height: 16.0),
              if (widget.task.types != null) ...[
                Wrap(
                  runSpacing: 12.0,
                  spacing: 12.0,
                  children: widget.task.types!
                      .map((type) => Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 4.0),
                            decoration: BoxDecoration(
                              color: Colors.white30,
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            child: Text(
                              type,
                              style: Get.textTheme.button!.copyWith(
                                color: Get.theme.primaryColorDark,
                              ),
                            ),
                          ))
                      .toList(),
                ),
              ],

              // the summary time
              // show the time, date of the task picked, and the priority of the task
              const SizedBox(height: 32.0),
              Row(
                children: [
                  Icon(
                    IconlyLight.calendar,
                    size: 18.0,
                    color: Get.theme.primaryColorDark,
                  ),
                  const SizedBox(width: 8.0),
                  Text(
                    decodeDeadline(widget.task.deadline!),
                    style: Get.textTheme.caption!.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Get.theme.primaryColorDark,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
