import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:intl/intl.dart';
import 'package:koso/models/notification_model.dart';
import 'package:koso/models/task_model.dart';
import 'package:koso/viewmodels/notification_viewmodel.dart';
import 'package:koso/views/task_view.dart';

class NotificationItemCard extends StatelessWidget {
  final NotificationDetailModel notification;

  const NotificationItemCard({
    Key? key,
    required this.notification,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(notification),
      onDismissed: (direction) {
        // we need to delete the notification appears
        Get.find<NotificationViewModel>().removeNotification(notification);
      },
      child: GestureDetector(
        onTap: () {
          // going detail into notification
          if (notification.topic == "task") {
            // we going into task view
            Get.to(
              () => const TaskView(),
              arguments: {
                "task": TaskModel.fromMap(
                    notification.data["id"] ?? "", notification.data),
              },
            );
          }
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
          child: Row(
            children: [
              // icon
              Container(
                width: 40.0,
                height: 40.0,
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: BorderRadius.circular(16.0),
                ),
                child: Center(
                  child: Builder(
                    builder: (context) {
                      if (notification.topic == "task") {
                        return const Icon(
                          IconlyLight.edit,
                          color: Colors.white,
                        );
                      }

                      return const Icon(
                        IconlyLight.notification,
                        color: Colors.white,
                      );
                    },
                  ),
                ),
              ),

              // the notification detail
              // contain the title, date, and summary
              const SizedBox(width: 16.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    notification.title!,
                    style: Get.textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 4.0),
                  Text(
                    notification.description!,
                    style: Get.textTheme.bodyText2,
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    DateFormat("EEEE, d MM yyyy").format(notification.date!),
                    style: Get.textTheme.caption,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
