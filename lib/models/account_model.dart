class AccountModel {
  String? id;
  String? avatar;
  String? email;
  String? fullName;
  String? bio;
  String? credential;
  AccountModel({
    this.id,
    this.avatar,
    this.email,
    this.fullName,
    this.bio,
    this.credential,
  });

  AccountModel copyWith({
    String? id,
    String? avatar,
    String? email,
    String? fullName,
    String? bio,
    String? credential,
  }) {
    return AccountModel(
      id: id ?? this.id,
      avatar: avatar ?? this.avatar,
      email: email ?? this.email,
      fullName: fullName ?? this.fullName,
      bio: bio ?? this.bio,
      credential: credential ?? this.credential,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'avatar': avatar,
      'email': email,
      'fullName': fullName,
      'bio': bio,
      'credential': credential,
    };
  }

  factory AccountModel.fromMap(String id, Map<String, dynamic> map) {
    return AccountModel(
      id: id,
      avatar: map['avatar'],
      email: map['email'],
      fullName: map['fullName'],
      bio: map['bio'],
      credential: map['credential'],
    );
  }

  @override
  String toString() {
    return 'AccountModel(id: $id, avatar: $avatar, email: $email, fullName: $fullName, bio: $bio, credential: $credential)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AccountModel &&
        other.id == id &&
        other.avatar == avatar &&
        other.email == email &&
        other.fullName == fullName &&
        other.bio == bio &&
        other.credential == credential;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        avatar.hashCode ^
        email.hashCode ^
        fullName.hashCode ^
        bio.hashCode ^
        credential.hashCode;
  }
}
