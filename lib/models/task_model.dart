import 'package:flutter/foundation.dart';

class TaskModel {
  String? id;
  String? accountId;
  String? title;
  String? description;
  List<String>? types;
  DateTime? deadline;
  String? color;
  List<String>? attachedFiles;
  String? status;
  TaskModel({
    this.id,
    this.accountId,
    this.title,
    this.description,
    this.types,
    this.deadline,
    this.color,
    this.attachedFiles,
    this.status,
  });

  TaskModel copyWith({
    String? id,
    String? accountId,
    String? title,
    String? description,
    List<String>? types,
    DateTime? deadline,
    String? color,
    List<String>? attachedFiles,
    String? status,
  }) {
    return TaskModel(
      id: id ?? this.id,
      accountId: accountId ?? this.accountId,
      title: title ?? this.title,
      description: description ?? this.description,
      types: types ?? this.types,
      deadline: deadline ?? this.deadline,
      color: color ?? this.color,
      attachedFiles: attachedFiles ?? this.attachedFiles,
      status: status ?? this.status,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'accountId': accountId,
      'title': title,
      'description': description,
      'types': types,
      'deadline': deadline?.millisecondsSinceEpoch,
      'color': color,
      'attachedFiles': attachedFiles,
      'status': status,
    };
  }

  factory TaskModel.fromMap(String id, Map<String, dynamic> map) {
    return TaskModel(
      id: id,
      accountId: map['accountId'],
      title: map['title'],
      description: map['description'],
      types: map['types'] != null ? List<String>.from(map['types']) : null,
      deadline: map['deadline'] != null
          ? DateTime.fromMillisecondsSinceEpoch(map['deadline'])
          : null,
      color: map['color'],
      attachedFiles: map['attachedFiles'] != null
          ? List<String>.from(map['attachedFiles'])
          : null,
      status: map['status'],
    );
  }

  @override
  String toString() {
    return 'TaskModel(id: $id, accountId: $accountId, title: $title, description: $description, types: $types, deadline: $deadline, color: $color, attachedFiles: $attachedFiles, status: $status)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TaskModel &&
        other.id == id &&
        other.accountId == accountId &&
        other.title == title &&
        other.description == description &&
        listEquals(other.types, types) &&
        other.deadline == deadline &&
        other.color == color &&
        listEquals(other.attachedFiles, attachedFiles) &&
        other.status == status;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        accountId.hashCode ^
        title.hashCode ^
        description.hashCode ^
        types.hashCode ^
        deadline.hashCode ^
        color.hashCode ^
        attachedFiles.hashCode ^
        status.hashCode;
  }
}
