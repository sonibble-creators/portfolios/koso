import 'dart:convert';

class NotificationDetailModel {
  String? topic;
  String? title;
  String? summary;
  String? description;
  DateTime? date;
  dynamic data;
  NotificationDetailModel({
    this.topic,
    this.title,
    this.summary,
    this.description,
    this.date,
    this.data,
  });

  NotificationDetailModel copyWith({
    String? topic,
    String? title,
    String? summary,
    String? description,
    DateTime? date,
    dynamic data,
  }) {
    return NotificationDetailModel(
      topic: topic ?? this.topic,
      title: title ?? this.title,
      summary: summary ?? this.summary,
      description: description ?? this.description,
      date: date ?? this.date,
      data: data ?? this.data,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'topic': topic,
      'title': title,
      'summary': summary,
      'description': description,
      'date': date?.millisecondsSinceEpoch,
      'data': data,
    };
  }

  factory NotificationDetailModel.fromMap(Map<String, dynamic> map) {
    return NotificationDetailModel(
      topic: map['topic'],
      title: map['title'],
      summary: map['summary'],
      description: map['description'],
      date: map['date'] != null
          ? DateTime.fromMillisecondsSinceEpoch(map['date'])
          : null,
      data: map['data'],
    );
  }

  String toJson() => json.encode(toMap());

  factory NotificationDetailModel.fromJson(String source) =>
      NotificationDetailModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'NotificationDetailModel(topic: $topic, title: $title, summary: $summary, description: $description, date: $date, data: $data)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is NotificationDetailModel &&
        other.topic == topic &&
        other.title == title &&
        other.summary == summary &&
        other.description == description &&
        other.date == date &&
        other.data == data;
  }

  @override
  int get hashCode {
    return topic.hashCode ^
        title.hashCode ^
        summary.hashCode ^
        description.hashCode ^
        date.hashCode ^
        data.hashCode;
  }
}
