import 'package:koso/configs/connection.dart';
import 'package:koso/models/account_model.dart';

class AccountService extends KosoConnection {
  // get the current user sign with email credential
  // identify
  // will return the account of user sign in
  Future<AccountModel> currentAccount(String email) async {
    return accountRef
        .where("email", isEqualTo: email)
        .limit(1)
        .get()
        .then((value) {
      if (value.docs.isNotEmpty) {
        return value.docs.first.data();
      }
      return AccountModel();
    });
  }

  // update an account
  // use to update the account data
  Future<AccountModel> updateAccount(AccountModel account) async {
    return accountRef.doc(account.id).set(account).then((value) {
      return account;
    }, onError: (err) {
      return AccountModel();
    });
  }

  // create account
  // create an account base on user email
  // this will call, if the current account not found
  Future<AccountModel> createAccount(AccountModel account) async {
    return accountRef.add(account).then(
      (value) {
        // get the data created
        return accountRef.doc(value.id).get().then((value) {
          if (value.exists) {
            return value.data()!;
          } else {
            return AccountModel();
          }
        }, onError: (err) {
          return AccountModel();
        });
      },
      onError: (err) {
        return AccountModel();
      },
    );
  }
}
