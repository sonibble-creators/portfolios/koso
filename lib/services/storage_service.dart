import 'dart:typed_data';

import 'package:koso/configs/connection.dart';

class StorageService extends KosoConnection {
  // upload some image we want
  // this upload using some path, and the data as Uint8List
  // this will return the download url, that can we use
  Future<String?> uploadImageData(String path, Uint8List data) async {
    return storage.ref(path).putData(data).then((res) {
      return res.ref.getDownloadURL();
    }, onError: (err) {
      return null;
    });
  }
}
