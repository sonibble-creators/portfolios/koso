import 'package:koso/configs/connection.dart';
import 'package:koso/models/task_model.dart';

class TaskService extends KosoConnection {
  // read all the task by its owner
  // show the all of task
  Future<List<TaskModel>> readAllTaskByOwner(String accountId) async {
    return taskRef
        .where("accountId", isEqualTo: accountId)
        // .orderBy("deadline")
        .get()
        .then((value) {
      if (value.docs.isNotEmpty) {
        return List<TaskModel>.from(value.docs.map((e) => e.data()).toList());
      } else {
        return List<TaskModel>.empty();
      }
    }, onError: (err) {
      return List<TaskModel>.empty();
    });
  }

  // add a new task
  Future<TaskModel> addTask(TaskModel newTask) async {
    return taskRef.add(newTask).then((value) {
      return value.get().then((value) {
        if (value.exists) {
          return value.data()!;
        } else {
          return TaskModel();
        }
      }, onError: (err) {
        return TaskModel();
      });
    }, onError: (err) {
      return TaskModel();
    });
  }

  // update task
  Future<TaskModel> updateTask(TaskModel newTask) async {
    return taskRef.doc(newTask.id).set(newTask).then((value) {
      return newTask;
    }, onError: (err) {
      return TaskModel();
    });
  }

  // delete task
  Future<void> deleteTask(TaskModel newTask) async {
    return taskRef.doc(newTask.id).delete();
  }
}
