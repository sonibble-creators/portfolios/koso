import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:koso/configs/connection.dart';

class AuthService extends KosoConnection {
  // get the current user sign into apps system
  // this will return null if its not signin
  Future<User?> currentUser() async {
    return Future.value(auth.currentUser);
  }

  // sign in
  // sign in using google
  Future<User?> signinWithGoogle() async {
    // when sign using google, we need to
    // take user using google sign into our application
    var googleAccount = await GoogleSignIn().signIn();

    // ok, now after get the account of google, we are going to
    // make it become credential, so we can sign using firebase
    if (googleAccount != null) {
      var googleAuth = await googleAccount.authentication;

      // get the credential from provider
      var authCredential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      // now we are going to sign using firebase
      // then we have the user credential
      var credential = await auth.signInWithCredential(authCredential);

      return credential.user;
    }

    // the signin process error, or failed
    // may there some problem
    return null;
  }

  // sign out
  Future<void> endSession() async {
    // first we gonna do is out form google
    // and we out from this app, certainly in firebase
    await GoogleSignIn().signOut();
    await auth.signOut();
  }
}
