import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:koso/components/alert.dart';

// the source of pick some image,
// this will show the modal dialog where to pick the image, and
// return it as `Xfile`, this can be null
Future<XFile?> pickImageSource() async {
  // variables
  XFile? file;

  // we need to pick some image from source
  // so we need to show the dialog first
  await Get.dialog(
    ImagePickSourceAlert(
      onCamera: () async {
        // pick image from camera
        file = await ImagePicker()
            .pickImage(source: ImageSource.camera, imageQuality: 5);

        // when this click we need to close the dialog first
        if (Get.isDialogOpen!) {
          await Future.delayed(const Duration(milliseconds: 200));
          Get.back();
        }
      },
      onGalery: () async {
        file = await ImagePicker()
            .pickImage(source: ImageSource.gallery, imageQuality: 5);

        // when this click we need to close the dialog first
        if (Get.isDialogOpen!) {
          await Future.delayed(const Duration(milliseconds: 200));
          Get.back();
        }
      },
    ),
  );

  // will return file, if the dialog closed
  return file;
}
