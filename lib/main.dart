import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:koso/app.dart';
import 'package:koso/viewmodels/account_viewmodel.dart';
import 'package:koso/viewmodels/common_viewmodel.dart';
import 'package:koso/viewmodels/notification_viewmodel.dart';
import 'package:koso/viewmodels/task_viewmodel.dart';

void main() async {
  // we need init some setting, and the configuration
  await initservice();

  // then, run the app
  runApp(const KosoApp());
}

// init service
Future<void> initservice() async {
  // ensure initialize, run service
  // binding all the flutter widget
  WidgetsFlutterBinding.ensureInitialized();

  // initialize the firebase app
  await Firebase.initializeApp();

  // initialize the date formating by locale
  // at first, we going to set locale into english, US
  var locale = const Locale("en", "US");
  if (Get.deviceLocale != null) {
    // ok, we going same depend on device locale
    locale = Get.deviceLocale!;
  }
  await initializeDateFormatting(locale.toString());

  // we are going to init some service
  // bind the viewmodel
  // register the viewmodel
  Get.put(CommonViewModel());
  await Get.put(AccountViewModel()).loadAccount();
  Get.put(TaskViewModel());
  Get.put(NotificationViewModel());
}
