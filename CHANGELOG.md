# CHANGELOG.md

## 1.0.1 (unreleased)

Features:

  - add support for custom field
  - add integration into much vendor such as gitlab, github, trello, and so on.

Bugfixs:

  - Notification not showing

## 1.0.0 (2021-11-26)

Main Release:

  - Release the MVP application